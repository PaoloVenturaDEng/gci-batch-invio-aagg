﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestioneConcorsi.Utils {
    public static class DateParser {

        public static string dataToDBFormat(this string date) {

            var year = date.Substring(0, 4);
            var month = date.Substring(5, 2);
            var day = date.Substring(8, 2);

            return day + "-" + month + "-" + year;
        }

        public static string dataToJSFormat(this string date) {

            var day = date.Substring(0, 2);
            var month = date.Substring(3, 2);
            var year = date.Substring(6, 4);

            return year + "-" + month + "-" + day;
        }

        public static string dataToOracleFormat(this string date) {
            string result = Convert.ToDateTime(date).ToString("MM/dd/yyyy");

            return result;
        }

    }

}
