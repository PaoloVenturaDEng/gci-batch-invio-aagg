﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

using System.IO;

using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using GestioneConcorsi.Models;
using GestioneConcorsi.DAO;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using GestioneConcorsi.Models.ViewModels;

namespace GestioneConcorsi.Utils
{
    public class DbUtility
    {
        public class InfoStatiDomanda
        {
            private bool _IsCompletata;

            private List<DateTime> _ElencoInvii;

            public bool IsCompletata
            {
                get
                {
                    return _IsCompletata;
                }
            }

            public bool IsInviata
            {
                get
                {
                    return _ElencoInvii.Any();
                }
            }

            public List<DateTime> ElencoInvii
            {
                get
                {
                    return _ElencoInvii;
                }
            }

            public DateTime? DataUltimoInvio
            {
                get
                {
                    DateTime? result = null;

                    if (_ElencoInvii.Any())
                    {
                        result = _ElencoInvii.OrderByDescending(x => x).FirstOrDefault();
                    }

                    return result;
                }
            }

            public InfoStatiDomanda(bool isCompletata, List<DateTime> elencoInvii)
            {
                _ElencoInvii = elencoInvii;

                _IsCompletata = isCompletata;
            }
        }

        static public InfoStatiDomanda GetInfoStatiDomanda(string idDomanda, string idDomandaConcorso, cConnection connection = null)
        {
            InfoStatiDomanda result = null;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                List<DateTime> elencoDate = new List<DateTime>();

                command = connection.GetCommand("SELECT DATAINS FROM TRL_DOMANDA_CONCORSO_PDF WHERE ID_DOMANDA_CONCORSO = :idDomandaConcorso ORDER BY DATAINS DESC");

                command.Parameters.Add(new OracleParameter("idDomandaConcorso", idDomandaConcorso));

                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        elencoDate.Add(dataReader.GetDateTime(0));
                    }
                }

                dataReader.Close();

                dataReader.Dispose();

                dataReader = null;

                command.Dispose();

                command = null;


                command = connection.GetCommand("SELECT INVIO_ABILITATO FROM TCO_DOMANDA WHERE ID_DOMANDA = :idDomanda");

                command.Parameters.Add("idDomanda", idDomanda);

                int INVIO_ABILITATO = Convert.ToInt32(command.ExecuteScalar());

                command.Dispose();

                command = null;

                result = new InfoStatiDomanda((INVIO_ABILITATO == 0 && elencoDate.Any()), elencoDate);
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetInfoStatiDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<GenericEntity> GetCertificazioniDomanda(string idDomanda, cConnection connection = null)
        {
            List<GenericEntity> result = new List<GenericEntity>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand(
                    "SELECT ID_CERTIFICAZIONE, DAL, AL, DURATA, ID_TIPOCERTIFICAZIONE, CODSEDE, CODDISTAC, ALTROSITO, ALTRO_TIPOCERTIFICAZIONE, RILASCIATODA, RILASCIATOIL, TIPO_DURATA " +
                    "FROM TCO_CERTIFICAZIONE " +
                    "WHERE ID_DOMANDA= :idDomanda ORDER BY ID_CERTIFICAZIONE", connection.Conn);

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        GenericEntity certificazione = new GenericEntity(GenericEntity.Tipologia.certificazione);

                        certificazione.ID_ENTITY = dataReader.GetInt32(0);
                        if (!dataReader.IsDBNull(1))
                            certificazione.DAL = dataReader.GetDateTime(1).ToShortDateString();
                        if (!dataReader.IsDBNull(2))
                            certificazione.AL = dataReader.GetDateTime(2).ToShortDateString();
                        if (!dataReader.IsDBNull(3))
                            certificazione.DURATA = dataReader.GetInt32(3);
                        if (!dataReader.IsDBNull(4))
                            certificazione.ID_TIPOENTITY = dataReader.GetInt32(4);
                        if (!dataReader.IsDBNull(5))
                            certificazione.CODSEDE = dataReader.GetString(5);
                        if (!dataReader.IsDBNull(6))
                            certificazione.CODDISTAC = dataReader.GetInt32(6);
                        if (!dataReader.IsDBNull(7))
                            certificazione.ALTROSITO = dataReader.GetString(7);
                        if (!dataReader.IsDBNull(8))
                            certificazione.ALTRO_TIPOENTITY = dataReader.GetString(8);
                        if (!dataReader.IsDBNull(9))
                            certificazione.RILASCIATODA = dataReader.GetString(9);
                        if (!dataReader.IsDBNull(10))
                            certificazione.RILASCIATOIL = dataReader.GetDateTime(10).ToShortDateString();
                        if (!dataReader.IsDBNull(11))
                            certificazione.TIPO_DURATA = dataReader.GetString(11);

                        result.Add(certificazione);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetCertificazioniDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<GenericEntity> GetBrevettiDomanda(string idDomanda, cConnection connection = null)
        {
            List<GenericEntity> result = new List<GenericEntity>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand(
                    "SELECT ID_BREVETTO, DAL, AL, DURATA, ID_TIPOBREVETTO, CODSEDE, CODDISTAC, ALTROSITO, ALTRO_TIPOBREVETTO, RILASCIATODA, RILASCIATOIL, TIPO_DURATA " +
                    "FROM TCO_BREVETTO " +
                    "WHERE ID_DOMANDA= :idDomanda ORDER BY ID_BREVETTO", connection.Conn);

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        GenericEntity brevetto = new GenericEntity(GenericEntity.Tipologia.brevetto);

                        brevetto.ID_ENTITY = dataReader.GetInt32(0);
                        if (!dataReader.IsDBNull(1))
                            brevetto.DAL = dataReader.GetDateTime(1).ToShortDateString();
                        if (!dataReader.IsDBNull(2))
                            brevetto.AL = dataReader.GetDateTime(2).ToShortDateString();
                        if (!dataReader.IsDBNull(3))
                            brevetto.DURATA = dataReader.GetInt32(3);
                        if (!dataReader.IsDBNull(4))
                            brevetto.ID_TIPOENTITY = dataReader.GetInt32(4);
                        if (!dataReader.IsDBNull(5))
                            brevetto.CODSEDE = dataReader.GetString(5);
                        if (!dataReader.IsDBNull(6))
                            brevetto.CODDISTAC = dataReader.GetInt32(6);
                        if (!dataReader.IsDBNull(7))
                            brevetto.ALTROSITO = dataReader.GetString(7);
                        if (!dataReader.IsDBNull(8))
                            brevetto.ALTRO_TIPOENTITY = dataReader.GetString(8);
                        if (!dataReader.IsDBNull(9))
                            brevetto.RILASCIATODA = dataReader.GetString(9);
                        if (!dataReader.IsDBNull(10))
                            brevetto.RILASCIATOIL = dataReader.GetDateTime(10).ToShortDateString();
                        if (!dataReader.IsDBNull(11))
                            brevetto.TIPO_DURATA = dataReader.GetString(11);

                        result.Add(brevetto);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetBrevettiDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<GenericEntity> GetPatentiDomanda(string idDomanda, cConnection connection = null)
        {
            List<GenericEntity> result = new List<GenericEntity>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand(
                    "SELECT ID_PATENTE, DAL, AL, DURATA, ID_TIPOPATENTE, CODSEDE, CODDISTAC, ALTROSITO, ALTRO_TIPOPATENTE, RILASCIATODA, RILASCIATOIL, TIPO_DURATA " +
                    "FROM TCO_PATENTE " +
                    "WHERE ID_DOMANDA= :idDomanda ORDER BY ID_PATENTE", connection.Conn);

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        GenericEntity patente = new GenericEntity(GenericEntity.Tipologia.patete);

                        patente.ID_ENTITY = dataReader.GetInt32(0);
                        if (!dataReader.IsDBNull(1))
                            patente.DAL = dataReader.GetDateTime(1).ToShortDateString();
                        if (!dataReader.IsDBNull(2))
                            patente.AL = dataReader.GetDateTime(2).ToShortDateString();
                        if (!dataReader.IsDBNull(3))
                            patente.DURATA = dataReader.GetInt32(3);
                        if (!dataReader.IsDBNull(4))
                            patente.ID_TIPOENTITY = dataReader.GetInt32(4);
                        if (!dataReader.IsDBNull(5))
                            patente.CODSEDE = dataReader.GetString(5);
                        if (!dataReader.IsDBNull(6))
                            patente.CODDISTAC = dataReader.GetInt32(6);
                        if (!dataReader.IsDBNull(7))
                            patente.ALTROSITO = dataReader.GetString(7);
                        if (!dataReader.IsDBNull(8))
                            patente.ALTRO_TIPOENTITY = dataReader.GetString(8);
                        if (!dataReader.IsDBNull(9))
                            patente.RILASCIATODA = dataReader.GetString(9);
                        if (!dataReader.IsDBNull(10))
                            patente.RILASCIATOIL = dataReader.GetDateTime(10).ToShortDateString();
                        if (!dataReader.IsDBNull(11))
                            patente.TIPO_DURATA = dataReader.GetString(11);

                        result.Add(patente);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetPatentiDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<GenericEntity> GetCorsiAggiornamentoDomanda(string idDomanda, cConnection connection = null)
        {
            List<GenericEntity> result = new List<GenericEntity>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand(
                    "SELECT ID_CORSOAGGIORN, DAL, AL, DURATA, ID_TIPOCORSO, CODSEDE, CODDISTAC, ALTROSITO, ALTRO_TIPOCORSO, RILASCIATODA, TIPO_DURATA " +
                    "FROM TCO_CORSOAGGIORN " +
                    "WHERE ID_DOMANDA= :idDomanda ORDER BY ID_CORSOAGGIORN", connection.Conn);

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        GenericEntity corso = new GenericEntity(GenericEntity.Tipologia.corso);

                        corso.ID_ENTITY = dataReader.GetInt32(0);
                        if (!dataReader.IsDBNull(1))
                            corso.DAL = dataReader.GetDateTime(1).ToShortDateString();
                        if (!dataReader.IsDBNull(2))
                            corso.AL = dataReader.GetDateTime(2).ToShortDateString();
                        if (!dataReader.IsDBNull(3))
                            corso.DURATA = dataReader.GetInt32(3);
                        if (!dataReader.IsDBNull(4))
                            corso.ID_TIPOENTITY = dataReader.GetInt32(4);
                        if (!dataReader.IsDBNull(5))
                            corso.CODSEDE = dataReader.GetString(5);
                        if (!dataReader.IsDBNull(6))
                            corso.CODDISTAC = dataReader.GetInt32(6);
                        if (!dataReader.IsDBNull(7))
                            corso.ALTROSITO = dataReader.GetString(7);
                        if (!dataReader.IsDBNull(8))
                            corso.ALTRO_TIPOENTITY = dataReader.GetString(8);
                        if (!dataReader.IsDBNull(9))
                            corso.RILASCIATODA = dataReader.GetString(9);
                        if (!dataReader.IsDBNull(10))
                            corso.TIPO_DURATA = dataReader.GetString(10);

                        result.Add(corso);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetCorsiAggiornamentoDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<TitoloDiStudio> GetTitoliDiStudioDomanda(string idDomanda, cConnection connection = null)
        {
            List<TitoloDiStudio> result = new List<TitoloDiStudio>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);


            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand(
                    "SELECT ID_TITOLODISTUDIO, ID_TIPOTITOLOSTUD, ISTITUTO, TITOLO, DATACONSEGUIMENTO " +
                    "FROM TCO_TITOLODISTUDIO " +
                    "WHERE ID_DOMANDA= :idDomanda ORDER BY ID_TITOLODISTUDIO", connection.Conn);

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        TitoloDiStudio titoloDiStudio = new TitoloDiStudio();

                        titoloDiStudio.ID_TITOLOSTUDIO = dataReader.GetInt32(0);

                        if (!dataReader.IsDBNull(1))
                            titoloDiStudio.ID_TIPOTITOLOSTUDIO = dataReader.GetInt32(1);

                        if (!dataReader.IsDBNull(2))
                            titoloDiStudio.ISTITUTO = dataReader.GetString(2);

                        if (!dataReader.IsDBNull(3))
                            titoloDiStudio.TITOLO = dataReader.GetString(3);

                        if (!dataReader.IsDBNull(4))
                        {
                            titoloDiStudio.DATACONSEGUIMENTO = dataReader.GetString(4);
                        }
                        else { titoloDiStudio.DATACONSEGUIMENTO = string.Empty; }

                        result.Add(titoloDiStudio);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetTitoloDiStudioDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public int GetVersioneDomanda(string idDomanda, cConnection connection = null)
        {
            int versioneDomanda = 0;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT COUNT(*) VERSIONE FROM TCO_DOMANDA_STO WHERE ID_DOMANDA = :idDomanda", connection.Conn);

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                versioneDomanda = Convert.ToInt32(command.ExecuteScalar()) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetVersioneDomanda()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return versioneDomanda;
        }

        static public int GetIdSatoDomandaCompletata(cConnection connection = null)
        {
            int idSatoDomandaCompletata = 0;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT ID_STATODOMANDA FROM TB0_STATODOMANDA WHERE TRIM(UPPER(STATODOMANDA))='COMPLETATA'", connection.Conn);

                idSatoDomandaCompletata = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetIdSatoDomandaCompletata()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return idSatoDomandaCompletata;
        }

        static public string GetDescrizioneSpecializzazione(string idSpecializzazione, cConnection connection = null)
        {
            string descrizioneSpecializzazione = string.Empty;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT TIPOSPECIALIZ FROM TB0_TIPOSPECIALIZ WHERE ID_TIPOSPECIALIZ = :idSpecializzazione", connection.Conn);

                command.Parameters.Add(new OracleParameter("idSpecializzazione", idSpecializzazione));

                descrizioneSpecializzazione = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetDescrizioneSpecializzazione()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return descrizioneSpecializzazione;
        }
    }

    public class PdfUtility
    {
        static public PdfDomandaModel GetPdfDomandaModel(ViewModelAnagrafica viewModelAnagrafica, ViewModelCarriera viewModelCarriera)
        {
            string idDomanda = viewModelAnagrafica.Anagrafica.ID_DOMANDA.ToString();

            PdfDomandaModel result = new PdfDomandaModel();

            cConnection connection = null;

            try
            {
                connection = new cConnection();

                result.DatiCarriera = viewModelCarriera.Carriera;


                result.ElencoTipiCorso = PdfUtility.GetTipiCorso();
                result.ElencoTipiBrevetto = PdfUtility.GetTipiBrevetto();
                result.ElencoTipiPatente = PdfUtility.GetTipiPatente();
                result.ElencoTipiCertificazione = PdfUtility.GetTipiCertificazione();
                result.ElencoTipiTitoloStudio = PdfUtility.GetTipiTitoloStudio();


                result.ElencoCertificazioni = DbUtility.GetCertificazioniDomanda(idDomanda, connection);

                result.ElencoBrevetti = DbUtility.GetBrevettiDomanda(idDomanda, connection);

                result.ElencoPatenti = DbUtility.GetPatentiDomanda(idDomanda, connection);

                result.ElencoCorsiAggiornamento = DbUtility.GetCorsiAggiornamentoDomanda(idDomanda, connection);

                result.ElencoTitoli = DbUtility.GetTitoliDiStudioDomanda(idDomanda, connection);

                List<string> elencoCodiciSediInteressate = new List<string>();
                elencoCodiciSediInteressate.Add(result.DatiCarriera.SEDESERVIZIO);
                foreach (var codiceSede in result.ElencoCertificazioni.Where(x => x.CODSEDE != null).Select(x => x.CODSEDE)) if (codiceSede.HasText()) elencoCodiciSediInteressate.Add(codiceSede);
                foreach (var codiceSede in result.ElencoBrevetti.Where(x => x.CODSEDE != null).Select(x => x.CODSEDE)) if (codiceSede.HasText()) elencoCodiciSediInteressate.Add(codiceSede);
                foreach (var codiceSede in result.ElencoPatenti.Where(x => x.CODSEDE != null).Select(x => x.CODSEDE)) if (codiceSede.HasText()) elencoCodiciSediInteressate.Add(codiceSede);
                foreach (var codiceSede in result.ElencoCorsiAggiornamento.Where(x => x.CODSEDE != null).Select(x => x.CODSEDE)) if (codiceSede.HasText()) elencoCodiciSediInteressate.Add(codiceSede);
                elencoCodiciSediInteressate = elencoCodiciSediInteressate.Distinct().ToList();

                result.ElencoSediDistaccamenti = PdfUtility.GetSediDistaccamenti(elencoCodiciSediInteressate);


                result.DatiAnagrafica = viewModelAnagrafica.Anagrafica;



                result.IdDomanda = idDomanda;

                result.VersioneDomanda = DbUtility.GetVersioneDomanda(idDomanda, connection).ToString();

                result.Specializzazione_Denominazione = DbUtility.GetDescrizioneSpecializzazione(result.DatiCarriera.ID_TIPOSPECIALIZ, connection).ToString();

                result.Specializzazione_DataConseguimento = result.DatiCarriera.DATASPECIALIZZAZIONE.HasText() ? Convert.ToDateTime(result.DatiCarriera.DATASPECIALIZZAZIONE) : DateTime.MinValue;

                result.Specializzazione_LuogoConseguimento = result.DatiCarriera.DATASPECIALIZZAZIONE.HasText() ? result.DatiCarriera.SEDESPECIALIZZAZIONE : string.Empty;

                result.DataInvio = DateTime.Now;

                result.DESCRIZIONE_SEDESERVIZIO = result.ElencoSediDistaccamenti.SingleOrDefault(x => x.CodiceSede == result.DatiCarriera.SEDESERVIZIO).DescrizioneSede;
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetPdfDomandaModel()", ex);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public DateTime OracleDateToDateTime(OracleDate oracleDate)
        {
            DateTime result = new DateTime();

            if(!oracleDate.IsNull)
                result = new DateTime(oracleDate.Year, oracleDate.Month, oracleDate.Day);

            return result;
        }

        static public string OracleDateToShortDateString(OracleDate oracleDate)
        {
            string result = string.Empty;

            if (!oracleDate.IsNull)
                result = OracleDateToDateTime(oracleDate).ToShortDateString();

            return result;
        }


        static public bool ManageNewPage(PdfWriter writer, Document doc)
        {
            bool newPage = (PdfUtility.GetPositionInPage(writer) <= 46.0f/*30.7f*/);

            if (newPage) doc.NewPage();

            return newPage;
        }

        static public float GetPositionInPage(PdfWriter pdfWriter)
        {
            float positionPoints = pdfWriter.GetVerticalPosition(false);

            float positionMillimetri = PdfUtility.PointsToMillimetri(positionPoints);

            return positionMillimetri;
        }

        static public void SendEmailOLD(byte[] attachmentContent)
        {
            try
            {
                string mittente = "r.fermanelli@digitalengineering.it";

                MailMessage myMail = new MailMessage(new MailAddress(mittente, "MITTente"), new MailAddress("p.ventura@digitalengineering.it", "pventure"));
                myMail.Subject = "oggetto...";
                myMail.Body = "corpo...";
                myMail.IsBodyHtml = false;

                SmtpClient mySmtpClient = new SmtpClient("smtp.digitalengineering.it", 587);
                mySmtpClient.EnableSsl = false;
                mySmtpClient.UseDefaultCredentials = true;
                mySmtpClient.Credentials = new System.Net.NetworkCredential(mittente, string.Empty);

                Attachment attachment = new Attachment(new MemoryStream(attachmentContent), "NOMEALLEGATO.pdf");
                myMail.Attachments.Add(attachment);

                mySmtpClient.Send(myMail);
            }

            catch (SmtpException ex)
            {
                throw new ApplicationException
                  ("SendEmail: SmtpException has occured: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo SendEmail()", ex);
            }
        }

        static public void SendEmail(
            string mittenteEmail, string mittenteDisplay,
            string destinatarioEmail, string destinatarioDisplay,
            string oggetto, string corpo, bool isHtml,
            string smtpClient, int porta,
            bool enableSsl,
            bool useDefaultCredentials,
            string username, string password,
            string nomeAllegato,
            byte[] attachmentContent,
            bool sendOnlyToMe
            )
        {
            try
            {
                if (mittenteDisplay == null || mittenteDisplay == string.Empty) mittenteDisplay = mittenteEmail;

                if (destinatarioDisplay == null || destinatarioDisplay == string.Empty) destinatarioDisplay = destinatarioEmail;

                MailMessage myMail = null;

                if (sendOnlyToMe)
                {
                    myMail = new MailMessage(new MailAddress(mittenteEmail, mittenteDisplay), new MailAddress(mittenteEmail, mittenteDisplay));
                }
                else
                {
                    myMail = new MailMessage(new MailAddress(mittenteEmail, mittenteDisplay), new MailAddress(destinatarioEmail, destinatarioDisplay));
                    myMail.Bcc.Add(new MailAddress(mittenteEmail, mittenteDisplay));
                }

                myMail.Subject = oggetto;
                myMail.Body = corpo;
                myMail.IsBodyHtml = isHtml;

                SmtpClient mySmtpClient = new SmtpClient(smtpClient, porta);
                mySmtpClient.EnableSsl = enableSsl;
                mySmtpClient.UseDefaultCredentials = useDefaultCredentials;
                mySmtpClient.Credentials = new System.Net.NetworkCredential(username, password);

                Attachment attachment = new Attachment(new MemoryStream(attachmentContent), nomeAllegato);

                myMail.Attachments.Add(attachment);

                mySmtpClient.Send(myMail);
            }

            catch (SmtpException ex)
            {
                throw new ApplicationException
                  ("SendEmail: SmtpException has occured: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo SendEmail()", ex);
            }
        }

        static public float MillimetriToInches(float millimetri)
        {
            float inches = millimetri * 0.0393701f;

            return inches;
        }

        static public float InchesToMillimetri(float inches)
        {
            float millimetri = inches / 0.0393701f;

            return millimetri;
        }

        static public float InchesToPoints(float inches)
        {
            float points = inches * 72.0f;

            return points;
        }

        static public float PointsToInches(float points)
        {
            float inches = points / 72.0f;

            return inches;
        }

        static public float MillimetriToPoints(float millimetri)
        {
            float points = InchesToPoints(MillimetriToInches(millimetri));

            return points;
        }

        static public float PointsToMillimetri(float points)
        {
            float millimetri = PointsToInches(InchesToMillimetri(points));

            return millimetri;
        }

        static public string Capital(string text)
        {
            string result = string.Empty;

            if (text.Length > 0)
            {
                string iniziale = text.Substring(0, 1).ToUpper();

                if (text.Length > 1)
                {
                    text = iniziale + text.Substring(1).ToLower();
                }
                else
                {
                    text = iniziale;
                }
            }

            return result;
        }

        static public Dictionary<int, string> GetTipiCorso()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            try
            {
                connection = new cConnection();

                dataReader = connection.GetDataReader("SELECT ID_TIPOCORSO, TIPOCORSO FROM TB0_TIPOCORSO");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        result.Add(Convert.ToInt32(dataReader[0]), Convert.ToString(dataReader[1]).Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetTipiCorso()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public Dictionary<int, string> GetTipiBrevetto()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            try
            {
                connection = new cConnection();

                dataReader = connection.GetDataReader("SELECT ID_TIPOBREVETTO, TIPOBREVETTO FROM TB0_TIPOBREVETTO");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        result.Add(Convert.ToInt32(dataReader[0]), Convert.ToString(dataReader[1]).Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetTipiBrevetto()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public Dictionary<int, string> GetTipiPatente()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            try
            {
                connection = new cConnection();

                dataReader = connection.GetDataReader("SELECT ID_TIPOPATENTE, TIPOPATENTE FROM TB0_TIPOPATENTE");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        result.Add(Convert.ToInt32(dataReader[0]), Convert.ToString(dataReader[1]).Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetTipiPatente()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public Dictionary<int, string> GetTipiCertificazione()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            try
            {
                connection = new cConnection();

                dataReader = connection.GetDataReader("SELECT ID_TIPOCERTIFICAZIONE, TIPOCERTIFICAZIONE FROM TB0_TIPOCERTIFICAZIONE");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        result.Add(Convert.ToInt32(dataReader[0]), Convert.ToString(dataReader[1]).Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetTipiCertificazione()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public string GetTipo(Dictionary<int, string> elencoTipi, GenericEntity genericEntity)
        {
            string DESCRIZIONE_TIPOOLD = elencoTipi[genericEntity.ID_TIPOENTITY];
            string DESCRIZIONE_TIPO = elencoTipi.SingleOrDefault(x => x.Key == genericEntity.ID_TIPOENTITY).Value;

            string result = DESCRIZIONE_TIPO == "ALTRO" ? genericEntity.ALTRO_TIPOENTITY : DESCRIZIONE_TIPO;

            return result;
        }

        static public Dictionary<int, PdfTipoTitoloStudio> GetTipiTitoloStudio()
        {
            Dictionary<int, PdfTipoTitoloStudio> result = new Dictionary<int, PdfTipoTitoloStudio>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            try
            {
                connection = new cConnection();

                dataReader = connection.GetDataReader("SELECT ID_TIPOTITOLOSTUD, TIPOTITOLOSTUD, ID_TSTU_TIPOLOGIA, DESC_CATEGORIA_TIT_STUD FROM VSY_TIPOTITOLOSTUD_EX");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        PdfTipoTitoloStudio pdfTipoTitoloStudio = new PdfTipoTitoloStudio(Convert.ToInt32(dataReader[0]), Convert.ToString(dataReader[1]).Trim(), dataReader[2] != DBNull.Value ? Convert.ToInt32(dataReader[2]) : 0, dataReader[2] != DBNull.Value ? Convert.ToString(dataReader[3]).Trim() : string.Empty);

                        result.Add(pdfTipoTitoloStudio.IdTipotitolostud, pdfTipoTitoloStudio);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetTipiTitoloStudio()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public PdfTipoTitoloStudio GetTipoTitoloStudio(Dictionary<int, PdfTipoTitoloStudio> elencoTipiTitoli, TitoloDiStudio titolo)
        {
            PdfTipoTitoloStudio resultOLD = elencoTipiTitoli[titolo.ID_TIPOTITOLOSTUDIO];

            PdfTipoTitoloStudio result = elencoTipiTitoli.SingleOrDefault(x => x.Key == titolo.ID_TIPOTITOLOSTUDIO).Value;

            return result;
        }

        static public List<PdfSede> GetSediDistaccamenti(List<string> elencoCodiciSediInteressate = null)
        {
            List<PdfSede> result = new List<PdfSede>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            try
            {
                connection = new cConnection();


                List<KeyValuePair<string, string>> elencoSedi = new List<KeyValuePair<string, string>>();

                dataReader = connection.GetDataReader("SELECT CODSEDE, DESCSEDE FROM TB0_SEDE");

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        elencoSedi.Add(new KeyValuePair<string, string>(Convert.ToString(dataReader[0]).Trim(), Convert.ToString(dataReader[1]).Trim()));
                    }
                }

                dataReader.Close();

                dataReader = null;

                foreach(var sede in elencoSedi)
                {
                        result.Add(new PdfSede(sede.Key, sede.Value));
                }


                foreach (var sede in result)
                {
                    if (elencoCodiciSediInteressate == null || elencoCodiciSediInteressate.Count == 0 || elencoCodiciSediInteressate.Contains(sede.CodiceSede))
                    {
                        try
                        {
                            command = new OracleCommand("SELECT CODDISTAC, DESCDISTAC FROM TB0_DISTACCAMENTO WHERE CODSEDE = :codSede", connection.Conn);

                            command.Parameters.Add(new OracleParameter("codSede", sede.CodiceSede));

                            dataReader = command.ExecuteReader();

                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    sede.ElencoDistaccamenti.Add(new PdfDistaccamento(sede.CodiceSede, Convert.ToInt32(dataReader[0]), Convert.ToString(dataReader[1]).Trim()));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Errore nel metodo PdfUtility:GetSediDistaccamenti()", ex);
                        }
                        finally
                        {
                            if (dataReader != null)
                            {
                                dataReader.Close();
                                dataReader = null;
                            }

                            if (command != null)
                            {
                                command.Dispose();

                                command = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetSediDistaccamenti()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public string GetSede(string codsede)
        {
            string result = string.Empty;

            cConnection connection = null;

            OracleCommand command = null;

            try
            {
                connection = new cConnection();

                command = new OracleCommand("SELECT DESCSEDE FROM TB0_SEDE WHERE CODSEDE = :codSede", connection.Conn);

                command.Parameters.Add(new OracleParameter("codSede", codsede));

                result = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetSede()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public string GetDistaccamento(string codSede, int codDistac)
        {
            string result = string.Empty;

            cConnection connection = null;

            OracleCommand command = null;

            try
            {
                connection = new cConnection();

                command = new OracleCommand("SELECT DESCDISTAC FROM TB0_DISTACCAMENTO WHERE CODSEDE = :codSede AND CODDISTAC = :codDistac", connection.Conn);

                command.Parameters.Add(new OracleParameter("codSede", codSede));

                command.Parameters.Add(new OracleParameter("codDistac", codDistac));

                result = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetDistaccamento()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public string GetLuogo(string codSede, int codDistac, string altroSito)
        {
            string presso = string.Empty;

            try
            {
                string sede = string.Empty;

                string distaccamento = string.Empty;

                if (codSede != null && codSede.Trim() != string.Empty)
                {
                    sede = GetSede(codSede.Trim()).Trim();

                    distaccamento = GetDistaccamento(codSede, codDistac).Trim();

                    presso = sede + (distaccamento != string.Empty ? " - " + distaccamento : string.Empty);
                }

                if (altroSito != null && altroSito.Trim() != string.Empty)
                {
                    if (presso == string.Empty)
                    {
                        presso = altroSito.Trim();
                    }
                    else
                    {
                        presso += " - " + altroSito.Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetLuogo()", ex);
            }

            return presso;
        }

        static public string GetLuogo(List<PdfSede> elencoSediDistaccamenti, string codSede, int codDistac, string altroSito)
        {
            string presso = string.Empty;

            try
            {
                string sede = string.Empty;

                string distaccamento = string.Empty;

                if (codSede != null && codSede.Trim() != string.Empty)
                {
                    PdfSede itemSede = elencoSediDistaccamenti.SingleOrDefault(x => x.CodiceSede == codSede.Trim());

                    sede = itemSede.DescrizioneSede.Trim();

                    if (codDistac > 0) distaccamento = itemSede.ElencoDistaccamenti.SingleOrDefault(x => x.Id == codDistac).Descrizione.Trim();

                    presso = sede + (distaccamento != string.Empty ? " - " + distaccamento : string.Empty);
                }

                if (altroSito != null && altroSito.Trim() != string.Empty)
                {
                    if (presso == string.Empty)
                    {
                        presso = altroSito.Trim();
                    }
                    else
                    {
                        presso += " - " + altroSito.Trim();
                    }
                }

                if (presso == string.Empty)
                {
                    presso = "________________________";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo PdfUtility:GetLuogo()", ex);
            }

            return presso;
        }
    }

    public class PdfTipoTitoloStudio
    {
        private int _IdTipotitolostud;
        public int IdTipotitolostud
        {
            get
            {
                return _IdTipotitolostud;
            }
        }
        private string _TipoTitoloStudio;
        public string TipoTitoloStudio
        {
            get
            {
                return _TipoTitoloStudio;
            }
        }
        private int _IdTstuTipologia;
        public int IdTstuTipologia
        {
            get
            {
                return _IdTstuTipologia;
            }
        }
        private string _DescrizioneCategoriaTitoloStudio;
        public string DescrizioneCategoriaTitoloStudio
        {
            get
            {
                return _DescrizioneCategoriaTitoloStudio;
            }
        }
        public PdfTipoTitoloStudio(int idTipotitolostud, string tipoTitoloStudio, int idTstuTipologia, string descrizioneCategoriaTitoloStudio)
        {
            _IdTipotitolostud = idTipotitolostud;

            _TipoTitoloStudio = tipoTitoloStudio;

            _IdTstuTipologia = idTstuTipologia;

            _DescrizioneCategoriaTitoloStudio = descrizioneCategoriaTitoloStudio;
        }
    }

    public class PdfDistaccamento
    {
        private string _CodiceSede;
        private int _IdDistaccamento;
        private string _DescrizioneDistaccamento;
        public string CodiceSede
        {
            get
            {
                return _CodiceSede;
            }
        }
        public int Id
        {
            get
            {
                return _IdDistaccamento;
            }
        }
        public string Descrizione
        {
            get
            {
                return _DescrizioneDistaccamento;
            }
        }
        public PdfDistaccamento(string codiceSede, int idDistaccamento, string descrizioneDistaccamento)
        {
            _CodiceSede = codiceSede;

            _IdDistaccamento = idDistaccamento;

            _DescrizioneDistaccamento = descrizioneDistaccamento;
        }
    }

    public class PdfSede
    {
        private string _CodiceSede;
        private string _DescrizioneSede;
        public string CodiceSede
        {
            get
            {
                return _CodiceSede;
            }
        }
        public string DescrizioneSede
        {
            get
            {
                return _DescrizioneSede;
            }
        }

        public List<PdfDistaccamento> ElencoDistaccamenti;

        public PdfSede(string codiceSede, string descrizioneSede)
        {
            ElencoDistaccamenti = new List<PdfDistaccamento>();

            _CodiceSede = codiceSede;

            _DescrizioneSede = descrizioneSede;
        }
    }

    public class PdfDomandaModel
    {
        public List<GenericEntity> ElencoCorsiAggiornamento;

        public List<GenericEntity> ElencoPatenti;

        public List<GenericEntity> ElencoBrevetti;

        public List<GenericEntity> ElencoCertificazioni;

        public List<TitoloDiStudio> ElencoTitoli;

        public GestioneConcorsi.Models.Carriera DatiCarriera;
        public string DESCRIZIONE_SEDESERVIZIO;

        public GestioneConcorsi.Models.Anagrafica DatiAnagrafica;

        public string IdDomanda;
        public string VersioneDomanda;
        public DateTime DataInvio;
        public string Specializzazione_Denominazione;
        public DateTime Specializzazione_DataConseguimento;
        public string Specializzazione_LuogoConseguimento;
        public DateTime dataAnzianitàDiServizio;

        public Dictionary<int, string> ElencoTipiCorso;
        public Dictionary<int, string> ElencoTipiBrevetto;
        public Dictionary<int, string> ElencoTipiPatente;
        public Dictionary<int, string> ElencoTipiCertificazione;
        public Dictionary<int, PdfTipoTitoloStudio> ElencoTipiTitoloStudio;
        public List<PdfSede> ElencoSediDistaccamenti;

        public PdfDomandaModel()
        {
            ElencoCorsiAggiornamento = new List<GenericEntity>();
            ElencoPatenti = new List<GenericEntity>();
            ElencoBrevetti = new List<GenericEntity>();
            ElencoCertificazioni = new List<GenericEntity>();
            ElencoTitoli = new List<TitoloDiStudio>();
            DatiCarriera = new Carriera();
            DatiAnagrafica = new Anagrafica();

        }
    }

    public class PdfFontUtility
    {
        public enum FontStyle
        {
            NORMAL = 0,
            BOLD = 1,
            ITALIC = 2,
            UNDERLINE = 4,
            STRIKETHRU = 8,
            BOLDITALIC = 3,
            BOLDUNDERLINE = 5,
        }

        static public BaseFont ArialBaseFont(FontStyle fontStyle = FontStyle.NORMAL)
        {
            string font_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "arial.ttf");

            BaseFont font_base = BaseFont.CreateFont(font_path, BaseFont.IDENTITY_H, false);

            return font_base;
        }

        static public Font ArialFont(float size, FontStyle fontStyle = FontStyle.NORMAL)
        {
            Font font = new iTextSharp.text.Font(ArialBaseFont(fontStyle), size, (int)fontStyle);

            return font;
        }

        static public BaseFont TimesNewRomanFontBaseFont(FontStyle fontStyle = FontStyle.NORMAL)
        {
            BaseFont font_base = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            return font_base;
        }

        static public Font TimesNewRomanFontFont(float size, FontStyle fontStyle = FontStyle.NORMAL)
        {
            Font font = new iTextSharp.text.Font(TimesNewRomanFontBaseFont(fontStyle), size, (int)fontStyle);

            return font;
        }
    }

    public class PdfTwoColumnHeaderFooter : PdfPageEventHelper
    {
        public PdfTwoColumnHeaderFooter() : base()
        {
            _Concorso = string.Empty;
        }

        // This is the contentbyte object of the writer
        PdfContentByte cb;
        // we will put the final number of pages in a template
        PdfTemplate template;
        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;
        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;
        #region Properties
        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Concorso;
        public string Concorso
        {
            get { return _Concorso; }
            set { _Concorso = value; }
        }

        private DateTime _DataInvio;
        public DateTime DataInvio
        {
            get { return _DataInvio; }
            set { _DataInvio = value; }
        }

        private string _IdDomanda;
        public string IdDomanda
        {
            get { return _IdDomanda; }
            set { _IdDomanda = value; }
        }

        private string _VersioneDomanda;
        public string VersioneDomanda
        {
            get { return _VersioneDomanda; }
            set { _VersioneDomanda = value; }
        }

        private float _AltezzaMargineIntestazione;
        public float AltezzaMargineIntestazione
        {
            get { return _AltezzaMargineIntestazione; }
            set { _AltezzaMargineIntestazione = value; }
        }

        private float _LarghezzaMargineIntestazione;
        public float LarghezzaMargineIntestazione
        {
            get { return _LarghezzaMargineIntestazione; }
            set { _LarghezzaMargineIntestazione = value; }
        }

        private float _AltezzaMarginePièDiPagina;
        public float AltezzaMarginePièDiPagina
        {
            get { return _AltezzaMarginePièDiPagina; }
            set { _AltezzaMarginePièDiPagina = value; }
        }

        private float _LarghezzaMarginePièDiPagina;
        public float LarghezzaMarginePièDiPagina
        {
            get { return _LarghezzaMarginePièDiPagina; }
            set { _LarghezzaMarginePièDiPagina = value; }
        }

        private string _HeaderLeft;
        public string HeaderLeft
        {
            get { return _HeaderLeft; }
            set { _HeaderLeft = value; }
        }
        private string _HeaderRight;
        public string HeaderRight
        {
            get { return _HeaderRight; }
            set { _HeaderRight = value; }
        }
        private Font _HeaderFont;
        public Font HeaderFont
        {
            get { return _HeaderFont; }
            set { _HeaderFont = value; }
        }
        private Font _FooterFont;
        public Font FooterFont
        {
            get { return _FooterFont; }
            set { _FooterFont = value; }
        }
        #endregion
        // we override the onOpenDocument method
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                //bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                bf = PdfFontUtility.ArialBaseFont();
                cb = writer.DirectContent;
                template = cb.CreateTemplate(PdfUtility.MillimetriToPoints(9), PdfUtility.MillimetriToPoints(9));
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);

            Rectangle pageSize = document.PageSize;

            string Domanda = (IdDomanda != string.Empty ? IdDomanda : string.Empty) + "/" + (VersioneDomanda != string.Empty ? VersioneDomanda : string.Empty);
            string DataInvioDomanda = DataInvio.ToString();

            if (Domanda != string.Empty)
            {
                cb.BeginText();

                cb.SetFontAndSize(bf, 12);

                cb.SetRGBColorFill(100, 100, 100);

                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Domanda, pageSize.GetLeft(document.LeftMargin + PdfUtility.MillimetriToPoints(LarghezzaMargineIntestazione)), pageSize.GetTop(PdfUtility.MillimetriToPoints(AltezzaMargineIntestazione)), 0);


                String textSpazioProtocollo = "[spazio per il protocollo]";

                float lenOfSpazioProtocollo = bf.GetWidthPoint(textSpazioProtocollo, 12);

                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, textSpazioProtocollo, pageSize.GetLeft(pageSize.Width / 2), pageSize.GetTop(PdfUtility.MillimetriToPoints(AltezzaMargineIntestazione)), 0);


                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, DataInvioDomanda, pageSize.GetRight(document.RightMargin + PdfUtility.MillimetriToPoints(LarghezzaMargineIntestazione)), pageSize.GetTop(PdfUtility.MillimetriToPoints(AltezzaMargineIntestazione)), 0);


                cb.EndText();
            }
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            Rectangle pageSize = document.PageSize;

            cb.SetRGBColorFill(100, 100, 100);

            cb.BeginText();
            cb.SetFontAndSize(bf, 12);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Concorso, pageSize.GetLeft(document.LeftMargin + PdfUtility.MillimetriToPoints(LarghezzaMarginePièDiPagina)), pageSize.GetBottom(PdfUtility.MillimetriToPoints(AltezzaMarginePièDiPagina)), 0);
            cb.EndText();

            float lenOf2Numbers = bf.GetWidthPoint("00", 12);

            int pageN = writer.PageNumber;
            String text = "Pagina " + pageN + " di ";
            float lenOfPage = bf.GetWidthPoint(text, 12);
            float lenOfPageAll = lenOfPage + lenOf2Numbers;
            cb.BeginText();
            cb.SetFontAndSize(bf, 12);
            cb.SetTextMatrix(pageSize.GetRight(document.RightMargin + PdfUtility.MillimetriToPoints(LarghezzaMarginePièDiPagina)) - lenOfPageAll, pageSize.GetBottom(PdfUtility.MillimetriToPoints(AltezzaMarginePièDiPagina)));
            cb.ShowText(text);
            cb.EndText();

            cb.AddTemplate(template, pageSize.GetRight(document.RightMargin + PdfUtility.MillimetriToPoints(LarghezzaMarginePièDiPagina)) - lenOf2Numbers, pageSize.GetBottom(PdfUtility.MillimetriToPoints(AltezzaMarginePièDiPagina)));

            float altezzaSpazioUtilizzato = 0;
            if (writer.PageNumber == 1) altezzaSpazioUtilizzato = 32f;
            float altezzaRettangoloBlu = document.PageSize.Height - document.TopMargin - PdfUtility.MillimetriToPoints(altezzaSpazioUtilizzato - 1.8f);
            Rectangle rettangoloBlu = new Rectangle(document.LeftMargin - PdfUtility.MillimetriToPoints(5), document.BottomMargin - PdfUtility.MillimetriToPoints(5), document.PageSize.Width - document.RightMargin + PdfUtility.MillimetriToPoints(5), altezzaRettangoloBlu + PdfUtility.MillimetriToPoints(5));
            rettangoloBlu.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
            rettangoloBlu.BorderWidth = 1;
            rettangoloBlu.BorderColor = new BaseColor(0, 0, 255);

            cb.SaveState();
            cb.SetColorStroke(new BaseColor(0, 0, 255));
            cb.Rectangle(rettangoloBlu);
            cb.Fill();
            cb.RestoreState();
        }
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            template.BeginText();
            template.SetFontAndSize(bf, 12);
            template.SetTextMatrix(0, 0);
            template.ShowText(string.Empty + (writer.PageNumber));
            template.EndText();
        }

        static private List<KeyValuePair<int, string>> GetComuni()
        {
            List<KeyValuePair<int, string>> result = null;

            cConnection connection = null;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                result = new List<KeyValuePair<int, string>>();

                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT CODCOMUNE, DESCCOMUNE || ' (' || CODPROV || ')' FROM TAN_COMUNE ORDER BY DESCCOMUNE", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(new KeyValuePair<int, string>(dataReader.GetInt32(0), dataReader.GetString(1)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<KeyValuePair<string, string>> GetSedi()
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            cConnection connection = null;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT CODSEDE, DESCSEDE FROM TB0_SEDE ORDER BY DESCSEDE", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(new KeyValuePair<string, string>(dataReader.GetString(0), dataReader.GetString(1)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }
    }
}
