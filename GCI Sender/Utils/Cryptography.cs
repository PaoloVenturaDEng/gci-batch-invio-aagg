﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace GestioneConcorsi.Utils
{
    public class Cryptography
    {
        static byte[] bytIV = { 45, 1, 56, 7, 103, 89, 45, 11 };
        static byte[] bytKey = { 69, 48, 95, 88, 57, 46, 5, 71 };

        static public string ByteArrayToString(ref byte[] bytes)
        {
            string Result = "";

            for (int i = 0; i < bytes.Length; i++)
            {
                Result += (char)bytes[i];
            }

            return Result;
        }

        static public byte[] StringToByteArray(string Stringa)
        {
            byte[] Result = new byte[Stringa.Length];

            for (int i = 0; i < Stringa.Length; i++)
            {
                Result[i] = (byte)Stringa[i];
            }

            return Result;
        }

        static public byte[] Crypt(string Stringa)
        {
            DES algDES = DES.Create();

            ICryptoTransform desEncryptor = algDES.CreateEncryptor(bytIV, bytKey);

            byte[] bytInput = StringToByteArray(Stringa);

            byte[] bytOutput = desEncryptor.TransformFinalBlock(bytInput, 0, bytInput.Length);

            return bytOutput;
        }

        static public string Decrypt(ref byte[] bytInput)
        {
            DES algDES = DES.Create();

            ICryptoTransform desDecryptor = algDES.CreateDecryptor(bytIV, bytKey);

            byte[] bytOutput = desDecryptor.TransformFinalBlock(bytInput, 0, bytInput.Length);

            return ByteArrayToString(ref bytOutput);
        }

        static public string CryptToText(string str)
        {
            string Result = "";

            if (str.Length > 0)
            {
                byte[] bytEncrypted = Crypt(str);

                for (int i = 0; i < bytEncrypted.Length; i++)
                {
                    int intVal = (int)bytEncrypted[i];

                    string strVal = intVal.ToString();

                    if (intVal < 10)
                    {
                        strVal = "00" + strVal;
                    }
                    else if (intVal < 100)
                    {
                        strVal = "0" + strVal;
                    }

                    Result += strVal;
                }
            }

            return Result;
        }

        static public string DecryptFromText(string strInput)
        {
            string Result = "";

            if (strInput.Length > 0)
            {
                int i = 0;

                byte[] bytInput = new byte[strInput.Length / 3];

                while (strInput.Length > 0)
                {
                    string strVal = strInput.Substring(0, 3);

                    int intVal = Convert.ToInt32(strVal);

                    bytInput[i] = (byte)intVal;

                    i++;

                    strInput = strInput.Substring(3);
                }

                Result = Decrypt(ref bytInput);
            }

            return Result;
        }


        static string[] ExtractComponents(string strInput, char Separator)
        {
            List<string> Result = new List<string>();

            string strInputBACKUP = strInput;


            while (strInput.Length > 0)
            {
                if (strInput[0] == Separator)
                {
                    string Separators = "";

                    while (strInput.Length > 0 && strInput[0] == Separator)
                    {
                        Separators += Separator;

                        strInput = strInput.Substring(1);
                    }

                    Result.Add(Separators);
                }
                else
                {
                    int SeparatorIndex = strInput.IndexOf(Separator);

                    if (SeparatorIndex >= 0)
                    {
                        Result.Add(strInput.Substring(0, SeparatorIndex));

                        strInput = strInput.Substring(SeparatorIndex);
                    }
                    else
                    {
                        Result.Add(strInput);

                        strInput = "";
                    }
                }
            }

            string[] Result2 = new string[Result.Count];

            string strInputTEST = "";

            for (int i = 0; i < Result.Count; i++)
            {
                Result2[i] = Result[i];

                strInputTEST += Result[i];
            }

            if (!strInputTEST.Equals(strInputBACKUP))
            {
                throw new Exception("ERRORE");
            }

            return Result2;
        }

        static public string CryptToTextWithSpaces(string strInput)
        {
            string Result = "";

            if (strInput.Length > 0)
            {
                char Separator = ' ';

                char Separatoreplacer = 'O';

                string[] strInputComponents = ExtractComponents(strInput, Separator);

                for (int i = 0; i < strInputComponents.Length; i++)
                {
                    if (strInputComponents[i][0] == Separator)
                    {
                        Result += strInputComponents[i].Replace(Separator, Separatoreplacer);
                    }
                    else
                    {
                        Result += CryptToText(strInputComponents[i]);
                    }
                }
            }

            return Result;
        }

        static public string DecryptFromTextWithSpaces(string strInput)
        {
            string Result = "";

            if (strInput.Length > 0)
            {
                char Separator = 'O';

                char Separatoreplacer = ' ';

                string[] strInputComponents = ExtractComponents(strInput, Separator);

                for (int i = 0; i < strInputComponents.Length; i++)
                {
                    if (strInputComponents[i][0] == Separator)
                    {
                        Result += strInputComponents[i].Replace(Separator, Separatoreplacer);
                    }
                    else
                    {
                        Result += DecryptFromText(strInputComponents[i]);
                    }
                }
            }

            return Result;
        }

        public static string DecriptConnectionString(string ConnectionString)
        {
            /*
            string Search = ";Password=";
            string Base = ConnectionString;
            int FoundIndex = Base.IndexOf(Search) + Search.Length;
            string Pwd = Base.Substring(FoundIndex);
            string Result = Base.Substring(0, FoundIndex) + Cryptography.DecryptFromTextWithSpaces(Pwd);
            */
            string Result = Cryptography.DecryptFromTextWithSpaces(ConnectionString);

            return Result;
        }
    }
}