﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GestioneConcorsi.Utils
{
    static public class Extensions
    {

        public static string Capital(this string text)
        {
            string result = text;

            if (text.HasText())
            {
                if (text.Length > 1)
                {
                    result = text.Substring(0, 1).ToUpper() + text.Substring(1).ToLower();
                }
                else
                {
                    result = text.ToUpper();
                }

            }

            return result;
        }

        public static string FullMessage(this Exception exception, string lineSeparator = " ")
        {
            string result = string.Empty;

            if (lineSeparator.IsNullOrEmpty())
            {
                lineSeparator = System.Environment.NewLine;
            }

            if (exception.InnerException != null)
                result = exception.InnerException.FullMessage();

            result += (result != string.Empty ? lineSeparator : string.Empty) + exception.Message + lineSeparator;

            return result;
        }

        public static bool IsNullOrEmpty(this string text)
        {
            return text == null || text.Trim().Equals(string.Empty);
        }

        public static bool HasText(this string text)
        {
            return !text.IsNullOrEmpty();
        }

        public static object ConvertToDbOracleDate(this string dateText)
        {
            object result = DBNull.Value;

            if (dateText.HasText())
                result = dateText.dataToOracleFormat();

            return result;
        }

        public static object ConvertToDbString(this string text)
        {
            object result = DBNull.Value;

            if (text.HasText())
                result = text;

            return result;
        }

        public static object ConvertToDbValidId(this int id)
        {
            object result = DBNull.Value;

            if (id > 0)
                result = id;

            return result;
        }

    }
}