﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;

/// <summary>
/// Questa classe si occupa dell'integrazione con Active Directory e la scocca maker.
/// </summary>
public class ActiveDirectoryAdapter
{
    static public bool IsValidUser(string username, string completeUserName, string password, ref string Messaggio)
    {
        bool Result = false;

        if (System.Configuration.ConfigurationManager.AppSettings["ActiveDirectory_Domain"] != null)
        {
            string ActiveDirectory_Domain = System.Configuration.ConfigurationManager.AppSettings["ActiveDirectory_Domain"].ToString();

            Result = DomainAuthenticationUtility.LogonUser(ActiveDirectory_Domain, username, password, ref Messaggio);
        }
        else
        {
            throw new ArgumentNullException("Il parametro 'ActiveDirectory_Domain' non esiste nel web.config");
        }

        return Result;
    }
}
