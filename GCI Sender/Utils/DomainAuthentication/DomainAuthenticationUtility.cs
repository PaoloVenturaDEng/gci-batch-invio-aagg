﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.DirectoryServices;
using System.Runtime.InteropServices;

/// <summary>
/// Questa classe si occupa dell'integrazione con Active Directory e la scocca maker.
/// </summary>
public class DomainAuthenticationUtility
{
    #region NTLogonUser
    #region Direct OS LogonUser Code
    [DllImport("advapi32.dll")]
    private static extern bool LogonUser(String lpszUsername,
            String lpszDomain, String lpszPassword, int dwLogonType,
            int dwLogonProvider, out int phToken);

    [DllImport("Kernel32.dll")]
    private static extern int GetLastError();

    public static bool LogonUser(String sDomain, String sUser, String sPassword, ref string Messaggio)
    {
       int token1, ret;
        int attmpts = 0;

        bool LoggedOn = false;

        while (Messaggio.Length == 0 && !LoggedOn && attmpts < 2)
        {
            LoggedOn = LogonUser(sUser + "@" + sDomain, null, sPassword, 3, 0, out token1);

            if (!LoggedOn)
            {
                switch (ret = GetLastError())
                {
                    case (126): 

                        if (attmpts++ > 2)
                        {
                            Messaggio = "Specified module could not be found. error code: " + ret.ToString();
                            Messaggio = "Il modulo specificato non è stato trovato. Codice d'errore: " + ret.ToString();
                        }
                        
                        break;

                    case (1314)://ERROR_PRIVILEGE_NOT_HELD : A required privilege is not held by the client.

                        Messaggio = "A required privilege is not held by the client.";
                        Messaggio = "Il client non possiede tutti i privilegi necessari.";

                        break;

                    case (1315)://ERROR_INVALID_ACCOUNT_NAME : The name provided is not a properly formed account name.

                        Messaggio = "The name provided is not a properly formed account name.";
                        Messaggio = "Il nome fornito non rispetta il formato richiesto per account name.";

                        break;

                    case (1317)://ERROR_NO_SUCH_USER : The specified user does not exist.
 
                        Messaggio = "The specified user does not exist.";
                        Messaggio = "L'utente specificato non esiste.";

                        break;

                    case (1326)://ERROR_LOGON_FAILURE : Logon failure: unknown user name or bad password.

                        Messaggio = "Logon failure: unknown user name or bad password.";
                        Messaggio = "Errore durante l'accesso: nome utente sconosciuto o password non valida.";

                        break;
                        
                    case (1327)://ERROR_ACCOUNT_RESTRICTION : Logon failure: user account restriction. Possible reasons are blank passwords not allowed, logon hour restrictions, or a policy restriction has been enforced.

                        Messaggio = "Logon failure: user account restriction. Possible reasons are blank passwords not allowed, logon hour restrictions, or a policy restriction has been enforced.";
                        Messaggio = "Errore durante l'accesso: restrizione sull'account utente. Possibili cause sono password vuota, restrizioni sull'ora di accesso o restrizioni legate ad una policy.";

                        break;

                    case (1328)://ERROR_INVALID_LOGON_HOURS : Logon failure: account logon time restriction violation.

                        Messaggio = "Logon failure: account logon time restriction violation.";
                        Messaggio = "Errore durante l'accesso: violazione di una restrizione sugli orari di accesso consentiti all'account.";

                        break;

                    case (1330)://ERROR_PASSWORD_EXPIRED : Logon failure: the specified account password has expired.

                        Messaggio = "Logon failure: the specified account password has expired.";
                        Messaggio = "Errore durante l'accesso: la password dell'account specificato è scaduta.";

                        break;

                    case (1331)://ERROR_ACCOUNT_DISABLED : Logon failure: account currently disabled.

                        Messaggio = "Logon failure: account currently disabled.";
                        Messaggio = "Errore durante l'accesso: account attualmente disattivato.";

                        break;

                    case (1907)://ERROR_PASSWORD_MUST_CHANGE : The user's password must be changed before logging on the first time.

                        Messaggio = "The user's password must be changed before logging on the first time.";
                        Messaggio = "La password dell'utente deve essere cambiata prima di accedere per la prima volta.";

                        break;

                    default:

                        Messaggio = "Unexpected Logon Failure. Contact Administrator";
                        Messaggio = "Accesso non riuscito imprevisto. Contattare l'amministratore.";

                        break;

                }
            }
        }

        return LoggedOn;
    }
    #endregion Direct Logon Code
    #endregion NTLogonUser
}