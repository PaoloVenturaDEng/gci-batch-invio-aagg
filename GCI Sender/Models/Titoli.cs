﻿using Oracle.ManagedDataAccess.Types;
using System.ComponentModel.DataAnnotations;

namespace GestioneConcorsi.Models
{
    public class Titoli
    {
        public int ID_ANAGRAFICA { get; set; }
        public string CODICE_FISCALE { get; set; }
        public string COGNOME { get; set; }
        public string NOME { get; set; }
        public string ID_TITOLO { get; set; }
        public string DESCR_TITOLO { get; set; }
        public int ID_INDIRIZZO { get; set; }
        public string DESCR_INDIRIZZO { get; set; }
        public int DURATA_ANNI { get; set; }
        public int ANNO_CONSEGUIMENTO { get; set; }
        public OracleDate DT_CONSEGUIMENTO { get; set; }
        public string ISTITUTO_CONSEGUIMENTO { get; set; }
        public int CODCOMUNE { get; set; }
        public int CODPROV { get; set; }
        public OracleDate DT_VALIDAZIONE { get; set; }
 
    }
    public class TitoloDiStudio
    {
        public int ID_TITOLOSTUDIO { get; set; }
        public int ID_TIPOTITOLOSTUDIO { get; set; }
        public string DATACONSEGUIMENTO { get; set; }
        public string ISTITUTO { get; set; }
        public string TITOLO { get; set; }
        public int ALLEGATI { get; set; }
        public int ID_DOMANDA { get; set; }
        public int ID_UTEINS { get; set; }
        public OracleDate DATAINS { get; set; }
        public int ID_UTEMOD { get; set; }
        public OracleDate DATAMOD { get; set; }
    }
}