﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestioneConcorsi.Models
{
    public class ViewModelRiepilogo
    {
        public GestioneConcorsi.Utils.DbUtility.InfoStatiDomanda InfoStatiDomanda;

        public ViewModelRiepilogo(GestioneConcorsi.Utils.DbUtility.InfoStatiDomanda infoStatiDomanda)
        {
            InfoStatiDomanda = infoStatiDomanda;
        }
    }
}