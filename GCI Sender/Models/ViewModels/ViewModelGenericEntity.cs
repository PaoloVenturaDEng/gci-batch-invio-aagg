﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GestioneConcorsi.Utils;

namespace GestioneConcorsi.Models.ViewModels
{
    public class ViewModelGenericEntity
    {
        public List<GenericEntity> ListaRecord { get; set; }

        public List<KeyValuePair<string, string>> ListaSedi { get; set; }

        public string SelectedSede { get; set; }
        public string SezionePrecedente = string.Empty;
        public string SezioneSuccessiva = string.Empty;
        public string SezioneCorrente = string.Empty;

        public string ActionForInsert_ControllerName = string.Empty;
        public string ActionForInsert_ActionName = string.Empty;
        public string ActionForUpdate_ControllerName = string.Empty;
        public string ActionForUpdate_ActionName = string.Empty;

        public string TipoTitolo = string.Empty;
        public string NomeEntity = string.Empty;
        public string NomeCampoDb_IdTipoEntity = string.Empty;
        public string NomeCampoDb_IdEntity = string.Empty;
        public string NomeCampoDb_AltroTipoEntity = string.Empty;
        public string NomeCampoDb_TipoEntity = string.Empty;

        public string TitoloPagina = string.Empty;

        public bool IsAltroDefault = false;

        public bool IsDistaccamentoDisplayed = false;

        public GenericEntity NuovaEntità;

        public GestioneConcorsi.Utils.DbUtility.InfoStatiDomanda InfoStatiDomanda;

        public ViewModelGenericEntity(string nomeEntity, bool isAltroDefault, bool isDistaccamentoDisplayed, int idTipoAltro)
        {
            NomeEntity = nomeEntity;

            IsAltroDefault = isAltroDefault;

            IsDistaccamentoDisplayed = isDistaccamentoDisplayed;

            ListaRecord = new List<GenericEntity>();

            ListaSedi = new List<KeyValuePair<string, string>>();

            NuovaEntità = new GenericEntity(NomeEntity);

            if(IsAltroDefault) NuovaEntità.ID_TIPOENTITY = idTipoAltro;

            InfoStatiDomanda = null;
        }
    }
}