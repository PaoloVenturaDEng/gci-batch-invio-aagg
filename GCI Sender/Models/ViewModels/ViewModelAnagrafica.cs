﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestioneConcorsi.Models.ViewModels
{
    public class ViewModelAnagrafica
    {
        public bool IsValidModel;
        public string DisabledModel
        {
            get {
                return IsValidModel ? "" : " disabled=\"disabled\"";
            }
        }

        public Anagrafica Anagrafica { get; set; }
        public List<KeyValuePair<int, string>> ListaComuni { get; set; }
        public string SezionePrecedente = string.Empty;
        public string SezioneSuccessiva = "Carriera";
        public string SezioneCorrente = "Anagrafica";
        public GestioneConcorsi.Utils.DbUtility.InfoStatiDomanda InfoStatiDomanda;

        public ViewModelAnagrafica()
        {
            Anagrafica = new Anagrafica();

            ListaComuni = new List<KeyValuePair<int, string>>();

            IsValidModel = false;

            InfoStatiDomanda = null;
        }
    }
}
