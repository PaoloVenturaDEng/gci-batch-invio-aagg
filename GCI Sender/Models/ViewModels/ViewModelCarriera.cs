﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestioneConcorsi.Models
{
    public class ViewModelCarriera
    {
        public List<KeyValuePair<string, string>> SediServizio { get; set; }
        public List<KeyValuePair<int, string>> Specializzazioni { get; set; }
        public Carriera Carriera { get; set; }
        public string SezionePrecedente = "Anagrafica";
        public string SezioneSuccessiva = "Titoli";
        public string SezioneCorrente = "Carriera";
        public GestioneConcorsi.Utils.DbUtility.InfoStatiDomanda InfoStatiDomanda;

        public ViewModelCarriera()
        {
            SediServizio = new List<KeyValuePair<string, string>>();
            Specializzazioni = new List<KeyValuePair<int, string>>();
            Carriera = new Carriera();
            InfoStatiDomanda = null;
        }
    }
}