﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestioneConcorsi.Models.ViewModels
{
    public class ViewModelTitoli
    {
        public List<Titoli> ListaTitoliStudio { get; set; }
        public string SezionePrecedente = "Carriera";
        public string SezioneSuccessiva = "Corsi";
        public string SezioneCorrente = "Titoli";
        public GestioneConcorsi.Utils.DbUtility.InfoStatiDomanda InfoStatiDomanda;

        public ViewModelTitoli()
        {
            ListaTitoliStudio = new List<Titoli>();

            InfoStatiDomanda = null;
        }
    }
}