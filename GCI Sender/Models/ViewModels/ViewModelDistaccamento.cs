﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestioneConcorsi.Models
{


    public class ViewModelDistaccamento
    {
        public List<KeyValuePair<int, string>> ListaDistaccamenti { get; set; }
    }
}