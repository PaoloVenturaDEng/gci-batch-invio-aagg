﻿using System;
using System.ComponentModel.DataAnnotations;
using GestioneConcorsi.Utils;

namespace GestioneConcorsi.Models
{
    public class Anagrafica
    {

        public string NOME { get; set; }
        public string COGNOME { get; set; }
        //public Oracle.ManagedDataAccess.Types.OracleDate DTNASCITA { get; set; }
        public string DTNASCITA { get; set; }
        public string COMUNENASC { get; set; }
        public string PROVINCIA { get; set; }
        public string DESCR_STATO_NASCITA { get; set; }
        public string CODCOMUNE { get; set; }
        public string INDIRIZZO { get; set; }
        public string CODFISCALE { get; set; }
        //public int TELEFONO1 { get; set; }
        public string TELEFONO1 { get; set; }
        public string PREFTEL1 { get; set; }
        public string EMAIL { get; set; }
        public string DESCCOMUNE { get; set; }
        public string PROVINCIANAS { get; set; }
        public string CODCOMUNENASC { get; set; }
        public string ID_QUALIFICA { get; set; }
        public string SESSO { get; set; }
        public string CAP { get; set; }
        public int ID_DOMANDA { get; set; }
        public int ID_DOMANDA_CONCORSO { get; set; }
        public int F1BLOCCATA { get; set; }
        public int F2AVVIATA { get; set; }

        public Anagrafica()
        {
            NOME = string.Empty;

            COGNOME = string.Empty;

            DTNASCITA = string.Empty;

            COMUNENASC = string.Empty;

            PROVINCIA = string.Empty;

            DESCR_STATO_NASCITA = string.Empty;

            CODCOMUNE = string.Empty;

            INDIRIZZO = string.Empty;

            CODFISCALE = string.Empty;

            TELEFONO1 = string.Empty;

            PREFTEL1 = string.Empty;

            EMAIL = string.Empty;

            DESCCOMUNE = string.Empty;

            PROVINCIANAS = string.Empty;

            CODCOMUNENASC = string.Empty;

            ID_QUALIFICA = string.Empty;

            SESSO = string.Empty;

            CAP = string.Empty;

            F1BLOCCATA = 0;

            F2AVVIATA = 0;
        }

        public string dataNascitaView()
        {
            string result = "aaaa-mm-dd";

            string year, month, day;

            try
            {
                day = DTNASCITA.Substring(0, 2);

                month = DTNASCITA.Substring(3, 2);

                year = DTNASCITA.Substring(6, 4);

                result = year + "-" + month + "-" + day;
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public string telefonoView()
        {
            if (PREFTEL1.HasText())
                return PREFTEL1.ToString() + " " + TELEFONO1.ToString();
            else
                return TELEFONO1.ToString();

        }

    }
}
