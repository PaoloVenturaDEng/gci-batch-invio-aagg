﻿using Oracle.ManagedDataAccess.Types;
using System.ComponentModel.DataAnnotations;
using System;

namespace GestioneConcorsi.Models
{
    public class Carriera
    {
        public string ANZIANITA { get; set; }
        public string SEDESERVIZIO { get; set; }
        public string ID_TIPOSPECIALIZ { get; set; }
        public string SEDESPECIALIZZAZIONE { get; set; }    
        public string DATASPECIALIZZAZIONE { get; set; }

        public Carriera()
        {
            ANZIANITA = string.Empty;
            SEDESERVIZIO = string.Empty;
            ID_TIPOSPECIALIZ = string.Empty;
            SEDESPECIALIZZAZIONE = string.Empty;
            DATASPECIALIZZAZIONE = string.Empty;
        }

        public string dataAnzianitaView()
        {
            string result = "aaaa-mm-dd";

            string year, month, day;

            try
            {
                day = ANZIANITA.Substring(0, 2);

                month = ANZIANITA.Substring(3, 2);

                year = ANZIANITA.Substring(6, 4);

                result = year + "-" + month + "-" + day;
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public string dataSpecializzazioneView()
        {
            string result = "aaaa-mm-dd";

            string year, month, day;

            try
            {
                day = DATASPECIALIZZAZIONE.Substring(0, 2);

                month = DATASPECIALIZZAZIONE.Substring(3, 2);

                year = DATASPECIALIZZAZIONE.Substring(6, 4);

                result = year + "-" + month + "-" + day;
            }
            catch (Exception ex)
            {
            }

            return result;

        }

    }

}