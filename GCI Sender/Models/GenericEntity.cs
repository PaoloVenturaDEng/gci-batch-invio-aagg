﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestioneConcorsi.Models
{
    public class GenericEntity
    {
        public int ID_ENTITY { get; set; }
        public int ID_DOMANDA { get; set; }
        public string DAL { get; set; }
        public string AL { get; set; }
        public int DURATA /*SETTIMANE*/ { get; set; }
        public string TIPO_DURATA /*SETTIMANE*/ { get; set; }
        public int ID_TIPOENTITY { get; set; }
        public int ID_UTEINS { get; set; }
        public string DATAINS { get; set; }
        public int ID_UTEMOD { get; set; }
        public string DATAMOD { get; set; }
        public int CONVALIDATO { get; set; }
        public string CODSEDE { get; set; }
        public int CODDISTAC { get; set; }
        public string ALTROSITO { get; set; }
        public string ALTRO_TIPOENTITY { get; set; }
        public string RILASCIATODA { get; set; }
        public string RILASCIATOIL { get; set; }
        public Tipologia TipologiaIstanza;

        public string TipoEntità
        {
            get
            {
                return GetTipologia(TipologiaIstanza);
            }
        }

        public enum Tipologia
        {
            Unknown,
            brevetto,
            corso,
            certificazione,
            patete,
            master,
        }

        private void Init()
        {
            DAL = string.Empty;
            AL = string.Empty;
            TIPO_DURATA = string.Empty;
            CODSEDE = string.Empty;
            ALTROSITO = string.Empty;
            ALTRO_TIPOENTITY = string.Empty;
            RILASCIATODA = string.Empty;
            RILASCIATOIL = string.Empty;
            DATAINS = string.Empty;
            DATAMOD = string.Empty;
        }

        public GenericEntity(Tipologia tipologia)
        {
            TipologiaIstanza = tipologia;

            Init();
        }

        public GenericEntity(string entityName)
        {
            TipologiaIstanza = GetTipologia(entityName);

            Init();
        }

        static public Tipologia GetTipologia(string entityName)
        {
            Tipologia tipologia = Tipologia.Unknown;

            if (entityName == "Brevetto") tipologia = Tipologia.brevetto;
            else if (entityName == "Corso di aggiornamento") tipologia = Tipologia.corso;
            else if (entityName == "Certificazione") tipologia = Tipologia.certificazione;
            else if (entityName == "Patente") tipologia = Tipologia.patete;
            else if (entityName == "Master") tipologia = Tipologia.master;
            else throw new Exception("Nome tipologia sconosciuto");

            return tipologia;
        }

        static public string GetTipologia(Tipologia tipologia)
        {
            string entityName = string.Empty;            

            if (tipologia == Tipologia.brevetto) entityName = "Brevetto";
            else if (tipologia == Tipologia.corso) entityName = "Corso di aggiornamento";
            else if (tipologia == Tipologia.certificazione) entityName = "Certificazione";
            else if (tipologia == Tipologia.patete) entityName = "Patente";
            else if (tipologia == Tipologia.master) entityName = "Master";

            return entityName;
        }
    }
    public class GenericEntityInAggiornamento : GenericEntity
    {
        public string NomeAttributo { get; set; }

        public GenericEntityInAggiornamento(Tipologia tipologia) : base(tipologia)
        {
        }

        public GenericEntityInAggiornamento(string entityName) : base(entityName)
        {
        }
    }


}