﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GestioneConcorsi.Utils;
using GestioneConcorsi.Models;
using GestioneConcorsi.Models.ViewModels;

using iTextSharp.text;
using iTextSharp.text.pdf;

using System.IO;
using System.Data;
using Oracle.ManagedDataAccess.Types;
using GestioneConcorsi.DAO;
using Oracle.ManagedDataAccess.Client;

using System.Web;


namespace GCI_Sender
{
    class Program
    {
        static void Main(string[] args)
        {
            //string codiceFiscale = "FDNMRA57P17I480G";
            //string codiceFiscale = "JDHFYTNGJH";
            //string codiceFiscale = "JDHFYTNGJH4";

            List<string> codiciFiscali = GetCodiciFiscali(null);

            foreach(string codiceFiscale in codiciFiscali) InviaAAGG(codiceFiscale);

            Console.ReadLine();
        }

        static public string successMessage = "Invio AAGG eseguito";


        static public void InviaAAGG(string codiceFiscale)
        {
            bool success = false;

            string errorMessage = string.Empty;

            cConnection connection = null;

            OracleCommand command = null;

            ViewModelAnagrafica viewModelAnagrafica = null;

            ViewModelCarriera viewModelCarriera = null;

            try
            {
                connection = new cConnection();

                connection.BeginTrans();

                viewModelAnagrafica = new ViewModelAnagrafica();

                viewModelAnagrafica.ListaComuni = GetComuni(connection);

                if (viewModelAnagrafica.ListaComuni.Count == 0) throw new Exception("Operazione annulata: impossibile recuperare la lista dei comuni");
 

                string annoDecorrenza = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ANNO_DECORRENZA"]);

                int idConcorso = GetIdConcorso(connection, "CS", Convert.ToInt32(annoDecorrenza));

                if (idConcorso == 0)
                {
                    throw new Exception("La procedura concorsuale Capo Squadra per l'anno " + annoDecorrenza + " non è stata definita nel sistema. Contattare l'amministratore di sistema.");
                }

                DateTime? dataNotValidConseguimentoTitoli = null;
                GetMaxConseguimentoTitoli(connection, "CS", Convert.ToInt32(annoDecorrenza), out dataNotValidConseguimentoTitoli);

                DateTime? dataFinePresentazione = null;
                bool concorsoConTerminiScaduti = GetConcorsoConTerminiScaduti(connection, "CS", Convert.ToInt32(annoDecorrenza), out dataFinePresentazione);


                bool concorsoAttivo = GetConcorsoAttivo(connection, "CS", Convert.ToInt32(annoDecorrenza));

                if (!concorsoAttivo)
                {
                    throw new Exception("La procedura concorsuale Capo Squadra per l'anno " + annoDecorrenza + " non è attualmente disponibile. Contattare l'amministratore di sistema.");
                }


                string idDomanda = string.Empty;

                string idDomandaConcorso = string.Empty;

                string idUtente = string.Empty;

                DateTime dataUltimaModifica = DateTime.Now;

                string accountName = GetAccountNameFromCF(connection, codiceFiscale, idConcorso, ref idDomanda, ref idDomandaConcorso, ref idUtente, ref dataUltimaModifica);

                if (!accountName.HasText() || !idDomanda.HasText() || !idDomandaConcorso.HasText()) throw new Exception("Non è stata trovata una domanda per il codice fiscale specificato");



                if (!GetAnagraficaFromTcoDomanda(connection, accountName, annoDecorrenza, idConcorso, idDomanda, idDomandaConcorso, dataNotValidConseguimentoTitoli, ref viewModelAnagrafica, ref viewModelCarriera))
                {
                    throw new Exception("Errore sconosciuto in GetAnagraficaFromTcoDomanda");
                }
                else
                {
                    PdfDomandaModel domandaModel = PdfUtility.GetPdfDomandaModel(viewModelAnagrafica, viewModelCarriera);

                    if (domandaModel == null) throw new Exception("Errore sconosciuto in GetPdfDomandaModel");


                    byte[] attachmentContent = GetPDF(domandaModel, dataUltimaModifica);
                    /*
                    string nomeFile = "C:\\Users\\p.ventura\\Desktop\\test" + DateTime.Now.ToString().Replace("/", "_").Replace(":", "_") + ".pdf";

                    var f = File.Create(nomeFile);

                    f.Write(attachmentContent, 0, attachmentContent.Length);

                    f.Close();
                    */


                    VerifySendAlloed(connection, domandaModel);


                    string nomeAllegato = idDomanda + "." + DbUtility.GetVersioneDomanda(idDomanda) + ".pdf";

                    //string destinatarioEmail = viewModelAnagrafica.Anagrafica.EMAIL; //???
                    string destinatarioEmail = accountName + "@vigilfuoco.it"; //???

                    string destinatarioDisplay = destinatarioEmail;

                    bool isHtml = false;

                    string username = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__MITTENTE_USERNAME"]).Trim();

                    string password = Cryptography.DecriptConnectionString(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__MITTENTE_PASSWORD"]).Trim());

                    string mittenteEmail = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__MITTENTE_EMAIL_ADDRESS"]).Trim();

                    string mittenteDisplay = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__MITTENTE_DISPLAY"]).Trim();

                    string oggetto = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__OGGETTO"]).Trim();

                    string corpo = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__TESTO_EMAIL"]).Trim();

                    string smtpClient = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__SMTPCLIENT"]).Trim();

                    bool useDefaultCredentials = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__USEDEFAULTCREDENTIALS"]).Trim().ToUpper().Equals("TRUE");

                    bool enableSsl = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__ENABLESSL"]).Trim().ToUpper().Equals("TRUE");

                    int porta = Convert.ToInt32(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__PORTA"]).Trim());

                    bool sendOnlyToMe = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["INVIO_AAGG__SEND_ONLY_TO_ME"]).Trim().ToUpper().Equals("TRUE");

                    PdfUtility.SendEmail(mittenteEmail, mittenteDisplay, destinatarioEmail, destinatarioDisplay, oggetto, corpo, isHtml, smtpClient, porta, enableSsl, useDefaultCredentials, username, password, nomeAllegato, attachmentContent, sendOnlyToMe);




                    int idSatoDomandaCompletata = DbUtility.GetIdSatoDomandaCompletata(connection);

                    command = connection.GetCommand("UPDATE TCO_DOMANDA SET DATAUMOD = sysdate, ID_UTEMOD = :idUtemod, INVIO_ABILITATO = '0', ID_STATODOMANDA = :idSatoDomandaCompletata WHERE ID_DOMANDA = :idDomanda");

                    command.Parameters.Add("idUtemod", Convert.ToInt32(idUtente));

                    command.Parameters.Add("idSatoDomandaCompletata", idSatoDomandaCompletata);

                    command.Parameters.Add("idDomanda", idDomanda);

                    command.ExecuteNonQuery();

                    command.Dispose();

                    command = null;






                    String block = " BEGIN " +
                                   "     INSERT INTO TRL_DOMANDA_CONCORSO_PDF (ID_DOMANDA_CONCORSO_PDF, ID_UTEINS, PDF_IMAGE, ID_DOMANDA_CONCORSO, ID_DOMANDA, ID_CONCORSO) " +
                                   "     VALUES (0, :idUtente, :blobToDb, :idDomandaConcorso, :idDomanda, :idConcorso); " +
                                   " END; ";

                    command = connection.GetCommand(block);
                    command.BindByName = true;

                    OracleParameter paramIdUtente = command.Parameters.Add("idUtente", OracleDbType.Int32);
                    paramIdUtente.Direction = ParameterDirection.Input;
                    paramIdUtente.Value = idUtente;

                    OracleParameter paramBlobToDb = command.Parameters.Add("blobToDb", OracleDbType.Blob);
                    paramBlobToDb.Direction = ParameterDirection.Input;
                    paramBlobToDb.Value = attachmentContent;

                    OracleParameter paramIdDomandaConcorso = command.Parameters.Add("idDomandaConcorso", OracleDbType.Int32);
                    paramIdDomandaConcorso.Direction = ParameterDirection.Input;
                    paramIdDomandaConcorso.Value = idDomandaConcorso;

                    OracleParameter paramIdDomanda = command.Parameters.Add("idDomanda", OracleDbType.Int32);
                    paramIdDomanda.Direction = ParameterDirection.Input;
                    paramIdDomanda.Value = idDomanda;

                    OracleParameter paramIdConcorso = command.Parameters.Add("idConcorso", OracleDbType.Int32);
                    paramIdConcorso.Direction = ParameterDirection.Input;
                    paramIdConcorso.Value = Convert.ToString(idConcorso);

                    command.ExecuteNonQuery();

                    command.Dispose();

                    command = null;


                    command = connection.GetCommand("UPDATE TMP_INVII_AAGG SET DATA_ULTIMA_ELABORAZIONE = sysdate, CODICE_ESITO = 1, MESSAGGIO_ESITO = '" + successMessage +  "' WHERE CF = :codiceFiscale");

                    command.Parameters.Add("codiceFiscale", codiceFiscale);

                    command.ExecuteNonQuery();

                    command.Dispose();

                    command = null;


                    success = true;

                    connection.CommitTrans();
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Operazione annullata - " + ex.Message;

                if (connection != null)
                {
                    connection.RollbackTrans();
                }
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null)
                {
                    connection.Dispose();

                    connection = null;
                }
            }


            if (success)
            {
                Trace(codiceFiscale, true);
            }
            else 
            {
                Trace(codiceFiscale, false, errorMessage);

                try
                {
                    connection = new cConnection();

                    command = connection.GetCommand("UPDATE TMP_INVII_AAGG SET DATA_ULTIMA_ELABORAZIONE = sysdate, CODICE_ESITO = 0, MESSAGGIO_ESITO = :errorMessage WHERE CF = :codiceFiscale");

                    command.Parameters.Add("errorMessage", errorMessage.Replace("'", "''"));

                    command.Parameters.Add("codiceFiscale", codiceFiscale);

                    command.ExecuteNonQuery();

                    command.Dispose();

                    command = null;
                }
                catch (Exception ex)
                {
                    Trace(codiceFiscale, false, "ERRORE DURANTE LA TRACCIATURA DEL CODICE D'ERRORE NEL DATABASE. " + ex.Message);
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Dispose();

                        connection = null;
                    }
                }
            }
        }

        static public void Trace(string codiceFiscale, bool success, string errorMessage = null)
        {
            try
            {
                if (errorMessage == null) errorMessage = string.Empty;

                var file = File.AppendText("inviaCS2018AAGG." + DateTime.Now.ToShortDateString().Replace("/", ".") + ".Output.txt");

                file.WriteLine(DateTime.Now.ToString() + " - CODICE FISCALE " + codiceFiscale);
                Console.WriteLine(DateTime.Now.ToString() + " - CODICE FISCALE " + codiceFiscale);

                file.WriteLine(DateTime.Now.ToString() + " - " + (success ? successMessage : errorMessage));
                Console.WriteLine(DateTime.Now.ToString() + " - " + (success ? successMessage : errorMessage));

                file.WriteLine();
                Console.WriteLine();

                file.WriteLine("---------------------------------------------------------");
                Console.WriteLine("---------------------------------------------------------");

                file.WriteLine();
                Console.WriteLine();

                file.Close();
            }
            catch (Exception ex)
            {
            }
        }

        static public void VerifySendAlloed(cConnection connection, PdfDomandaModel domandaModel)
        {
            string idDomanda = domandaModel.DatiAnagrafica.ID_DOMANDA.ToString();

            string idDomandaConcorso = domandaModel.DatiAnagrafica.ID_DOMANDA_CONCORSO.ToString();


            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();



                command = new OracleCommand("SELECT COUNT(*) FROM TRL_DOMANDA_CONCORSO_PDF WHERE ID_DOMANDA = :idDomanda AND ID_DOMANDA_CONCORSO = :idDomandaConcorso", connection.Conn);

                command.Parameters.Add("idDomanda", idDomanda);

                command.Parameters.Add("idDomandaConcorso", idDomandaConcorso);

                int numeroInviiPrecedenti = Convert.ToInt32(command.ExecuteScalar());

                command.Dispose();

                command = null;



                command = new OracleCommand("SELECT INVIO_ABILITATO FROM TCO_DOMANDA WHERE ID_DOMANDA = :idDomanda", connection.Conn);

                command.Parameters.Add("idDomanda", idDomanda);

                int invioAbilitato = Convert.ToInt32(command.ExecuteScalar());

                command.Dispose();

                command = null;

                if (numeroInviiPrecedenti > 0)
                {
                    if (invioAbilitato == 0) throw new Exception("Non ci sono modifiche rispetto al precedente invio ad AAGG");
                    else throw new Exception("Esiste già un precedente invio ad AAGG (ci sono modifiche rispetto al precedente invio)");
                }

                if (invioAbilitato == 0)
                {
                    throw new Exception("Il pulsante 'Invia ad AAGG' risulta disabilitato: verificare la causa");
                }


                #region Controlli sui dati anagrafici

                if (!domandaModel.DatiAnagrafica.CODCOMUNE.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: codice Comune di domicilio");

                if (!domandaModel.DatiAnagrafica.INDIRIZZO.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: indirizzo di domicilio");

                if (!domandaModel.DatiAnagrafica.DESCCOMUNE.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: comune di domicilio");

                if (!domandaModel.DatiAnagrafica.PROVINCIA.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: provincia di domicilio");

                if (!domandaModel.DatiAnagrafica.CAP.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: C.A.P. di domicilio");

                //if (!domandaModel.DatiAnagrafica.TELEFONO1.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: telefono");

                //if (!domandaModel.DatiAnagrafica.EMAIL.HasText()) throw new Exception("Campo mancante nella sezione ANAGRAFICA: indirizzo di posta elettronica");

                #endregion


                #region Controlli sui dati di carriera

                command = new OracleCommand("SELECT ID_TIPOSPECIALIZ FROM TB0_TIPOSPECIALIZ WHERE upper(trim(TIPOSPECIALIZ))='NESSUNA'", connection.Conn);

                int idNessunaSpecializzazione = Convert.ToInt32(command.ExecuteScalar());

                command.Dispose();

                command = null;

                if (!domandaModel.DatiCarriera.ANZIANITA.HasText()) throw new Exception("Campo mancante nella sezione CARRIERA: data di anzianità");

                if (!domandaModel.DatiCarriera.SEDESERVIZIO.HasText()) throw new Exception("Campo mancante nella sezione CARRIERA: sede di servizio");

                if (!domandaModel.DatiCarriera.ID_TIPOSPECIALIZ.HasText())
                {
                    throw new Exception("Campo mancante nella sezione CARRIERA: tipo di specializzazione");
                }
                else if (domandaModel.DatiCarriera.ID_TIPOSPECIALIZ != idNessunaSpecializzazione.ToString())
                {
                    if (!domandaModel.DatiCarriera.DATASPECIALIZZAZIONE.HasText()) throw new Exception("Campo mancante nella sezione CARRIERA: data di specializzazione");

                    if (!domandaModel.DatiCarriera.SEDESPECIALIZZAZIONE.HasText()) throw new Exception("Campo mancante nella sezione CARRIERA: sede di specializzazione");
                }

                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }
        }

        static public bool GetAnagraficaFromTcoDomanda(cConnection connection, string accountName, string annoDecorrenza, 
            int idConcorso, string idDomanda, string idDomandaConcorso, 
            DateTime? dataNotValidConseguimentoTitoli, ref ViewModelAnagrafica viewModelAnagrafica, ref ViewModelCarriera viewModelCarriera)
        {
            bool result = false;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                string descrComuneNascita = string.Empty;

                string descrProvinciaNascita = string.Empty;

                string descrStatoNascita = string.Empty;

                string codComuneNascita = string.Empty;

                string codStatoNascita = string.Empty;

                GetInfoNascita(connection, idDomanda, ref codComuneNascita, ref descrComuneNascita, ref descrProvinciaNascita, ref codStatoNascita, ref descrStatoNascita);


                string sqlDatiCriptati = "SELECT " +
                    "TCO_DOMANDA.CODFISCALE, TCO_DOMANDA.COGNOME, TCO_DOMANDA.NOME, TAN_ANAGRAFICASIA.SESSO, " +
                    "TCO_DOMANDA.DOMICILIO_INDIRIZZO INDIRIZZO_DOMICILIO, " +
                    "DOMICILIO_CODCOMUNE COD_COMUNE_DOMICILIO, " +
                    "TAN_COMUNE.DESCCOMUNE DESCR_COMUNE_DOMICILIO, " +
                    "TCO_DOMANDA.DOMICILIO_CAP CAP_DOMICILIO, " +
                    "TAN_COMUNE.CODPROV SIGLA_PROVINCIA_DOMICILIO, " +
                    "TCO_DOMANDA.TELEFONO, '' PREFISSO_VUOTO, TCO_DOMANDA.EMAIL, " +
                    "TCO_DOMANDA.DATANASCITA, " +
                    "'" + Cryptography.CryptToTextWithSpaces(codComuneNascita) + "' COD_COMUNE_NASCITA, " +
                    "'" + Cryptography.CryptToTextWithSpaces(descrComuneNascita) + "' DESCR_COMUNE_NASCITA, " +
                    "TCO_DOMANDA.PROVINCIANASCITA SIGLA_PROVINCIA_NASCITA, " +
                    "'" + Cryptography.CryptToTextWithSpaces(descrStatoNascita) + "' DESCR_STATO_NASCITA, " +
                    "TB0_QUALIFICA.ID_QUALIFICA, TCO_DOMANDA.ID_DOMANDA, TCO_DOMANDA.F1BLOCCATA, TCO_DOMANDA.F2AVVIATA " +
                    "FROM TCO_DOMANDA " +
                    "JOIN TAN_COMUNE ON TAN_COMUNE.CODCOMUNE = TCO_DOMANDA.DOMICILIO_CODCOMUNE " +
                    "JOIN TRL_DOMANDA_CONCORSO on TRL_DOMANDA_CONCORSO.ID_DOMANDA = TCO_DOMANDA.ID_DOMANDA " +
                    "JOIN TB0_CONCORSO on TB0_CONCORSO.ID_CONCORSO = TRL_DOMANDA_CONCORSO.ID_CONCORSO " +
                    "JOIN TB0_PROCEDURA on TB0_PROCEDURA.ID_PROCEDURA = TB0_CONCORSO.ID_PROCEDURA " +
                    "JOIN TAN_ANAGRAFICASIA ON TAN_ANAGRAFICASIA.ACCOUNTNAME = :accountNameSia " +
                    "JOIN TB0_QUALIFICA ON TB0_QUALIFICA.CODQUALIFICA = TAN_ANAGRAFICASIA.CODQUALIFICA " +
                    "WHERE TCO_DOMANDA.ACCOUNTNAME = :accountName AND TB0_PROCEDURA.TIPO_CONCORSO = 'CS' AND EXTRACT(YEAR FROM TB0_CONCORSO.DATADECORRENZA) = :annoDecorrenza";

                command = new OracleCommand(sqlDatiCriptati, connection.Conn);

                command.BindByName = true;

                command.Parameters.Add(new OracleParameter("accountName", Cryptography.CryptToTextWithSpaces(accountName)));

                command.Parameters.Add(new OracleParameter("accountNameSia", accountName));

                command.Parameters.Add(new OracleParameter("annoDecorrenza", annoDecorrenza));

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        if (dataReader.Read())
                        {
                            viewModelAnagrafica.Anagrafica = GetAnagrafica(dataReader, true);

                            result = true;
                        }
                    }

                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                command.Dispose();

                command = null;




                if (result)
                {
                    result = false;

                    viewModelCarriera = new ViewModelCarriera();

                    command = connection.GetCommand("SELECT ID_TIPOSPECIALIZ, CODSEDE, DATAANZIANITA,  LUOGOSPECIALIZ, DATA_SPECIALIZZAZIONE  FROM TCO_DOMANDA WHERE ID_DOMANDA = :idDomanda");

                    command.BindByName = true;

                    command.Parameters.Add(new OracleParameter("idDomanda", viewModelAnagrafica.Anagrafica.ID_DOMANDA.ToString()));

                    dataReader = command.ExecuteReader();

                    if (dataReader != null)
                    {
                        if (dataReader.HasRows)
                        {
                            if (dataReader.Read())
                            {
                                if (!dataReader.IsDBNull(0)) viewModelCarriera.Carriera.ID_TIPOSPECIALIZ = dataReader.GetInt32(0).ToString();

                                if (!dataReader.IsDBNull(1)) viewModelCarriera.Carriera.SEDESERVIZIO = dataReader.GetString(1);

                                if (!dataReader.IsDBNull(2)) viewModelCarriera.Carriera.ANZIANITA = dataReader.GetDateTime(2).ToShortDateString();

                                if (!dataReader.IsDBNull(3)) viewModelCarriera.Carriera.SEDESPECIALIZZAZIONE = dataReader.GetString(3).ToUpper();

                                if (!dataReader.IsDBNull(4)) viewModelCarriera.Carriera.DATASPECIALIZZAZIONE = dataReader.GetDateTime(4).ToShortDateString();

                                result = true;
                            }
                        }

                        dataReader.Close();

                        dataReader.Dispose();

                        dataReader = null;
                    }

                    command.Dispose();

                    command = null;


                    viewModelAnagrafica.Anagrafica.ID_DOMANDA_CONCORSO = int.Parse(idDomandaConcorso);


                    viewModelCarriera.Specializzazioni = GetTipologieSpecializzazioni(connection);

                    viewModelCarriera.SediServizio = GetElencoSedi(connection);

                    viewModelCarriera.InfoStatiDomanda = DbUtility.GetInfoStatiDomanda(viewModelAnagrafica.Anagrafica.ID_DOMANDA.ToString(), idDomandaConcorso, connection);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetAnagraficaFromTcoDomanda()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }


        static public bool GetInfoNascita(cConnection connection,
            string idDomanda,
            ref string codComuneNascita,
            ref string descrComuneNascita,
            ref string descrProvinciaNascita,
            ref string codStatoNascita,
            ref string descrStatoNascita
            )
        {
            bool result = false;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();


                command = new OracleCommand("SELECT DISTINCT TCO_DOMANDA.CODCOMUNENASCITA, TCO_DOMANDA.CODSTATONASCITA, TCO_DOMANDA.PROVINCIANASCITA FROM TCO_DOMANDA WHERE TCO_DOMANDA.ID_DOMANDA = :idDomanda", connection.Conn);

                command.BindByName = true;

                command.Parameters.Add(new OracleParameter("idDomanda", idDomanda));

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        if (dataReader.Read())
                        {
                            descrComuneNascita = Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(0));

                            descrStatoNascita = Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(1));

                            descrProvinciaNascita = Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(2));
                        }
                    }

                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                command.Dispose();

                command = null;

                if (descrComuneNascita != string.Empty && descrStatoNascita != string.Empty)
                {
                    command = new OracleCommand("SELECT CODCOMUNE FROM TAN_COMUNE WHERE DESCCOMUNE = :desccomune AND CODPROV = :descrProvinciaNascita", connection.Conn);

                    command.BindByName = true;

                    command.Parameters.Add(new OracleParameter("desccomune", descrComuneNascita));

                    command.Parameters.Add(new OracleParameter("descrProvinciaNascita", descrProvinciaNascita));

                    codComuneNascita = Convert.ToString(command.ExecuteScalar());

                    command.Dispose();

                    command = null;



                    command = new OracleCommand("SELECT CODSTATO FROM TAN_STATOESTERO WHERE DESCSTATO = :descstato", connection.Conn);

                    command.BindByName = true;

                    command.Parameters.Add(new OracleParameter("descstato", descrStatoNascita));

                    codStatoNascita = Convert.ToString(command.ExecuteScalar());

                    command.Dispose();

                    command = null;

                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetInfoNascita()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public Anagrafica GetAnagrafica(OracleDataReader dataReader, bool decryptData)
        {
            Anagrafica result = new Anagrafica();

            try
            {
                result.CODFISCALE = dataReader.IsDBNull(0) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(0)) : dataReader.GetString(0));

                result.COGNOME = dataReader.IsDBNull(1) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(1)) : dataReader.GetString(1));

                result.NOME = dataReader.IsDBNull(2) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(2)) : dataReader.GetString(2));

                result.SESSO = dataReader.IsDBNull(3) ? string.Empty : dataReader.GetString(3);

                result.INDIRIZZO = dataReader.IsDBNull(4) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(4)) : dataReader.GetString(4));
                result.INDIRIZZO = result.INDIRIZZO.ToUpper();

                result.CODCOMUNE = dataReader.IsDBNull(5) ? string.Empty : dataReader.GetInt32(5).ToString();

                result.DESCCOMUNE = dataReader.IsDBNull(6) ? string.Empty : dataReader.GetString(6);

                result.CAP = dataReader.IsDBNull(7) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(7)) : dataReader.GetString(7));
                result.CAP = result.CAP.ToUpper();

                result.PROVINCIA = dataReader.IsDBNull(8) ? string.Empty : dataReader.GetString(8);

                result.TELEFONO1 = dataReader.IsDBNull(9) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(9)) : dataReader.GetInt64(9).ToString());
                result.TELEFONO1 = result.TELEFONO1.ToUpper();

                result.PREFTEL1 = dataReader.IsDBNull(10) ? string.Empty : (decryptData ? string.Empty : dataReader.GetString(10));
                result.PREFTEL1 = result.PREFTEL1.ToUpper();

                result.EMAIL = dataReader.IsDBNull(11) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(11)) : dataReader.GetString(11));
                result.EMAIL = result.EMAIL.ToLower();

                result.DTNASCITA = dataReader.IsDBNull(12) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(12)) : dataReader.GetDateTime(12).ToShortDateString());

                result.CODCOMUNENASC = dataReader.IsDBNull(13) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(13)) : dataReader.GetInt32(13).ToString());

                result.COMUNENASC = dataReader.IsDBNull(14) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(14)) : dataReader.GetString(14));

                result.PROVINCIANAS = dataReader.IsDBNull(15) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(15)) : dataReader.GetString(15));

                result.DESCR_STATO_NASCITA = dataReader.IsDBNull(16) ? string.Empty : (decryptData ? Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(16)) : dataReader.GetString(16));

                result.ID_QUALIFICA = dataReader.IsDBNull(17) ? string.Empty : dataReader.GetInt32(17).ToString();

                result.ID_DOMANDA = dataReader.IsDBNull(18) ? -1 : dataReader.GetInt32(18);

                result.F1BLOCCATA = dataReader.IsDBNull(19) ? -1 : dataReader.GetInt32(19);

                result.F2AVVIATA = dataReader.IsDBNull(20) ? -1 : dataReader.GetInt32(20);
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetAnagrafica()", ex);
            }
            finally
            {
            }


            return result;
        }


        static public byte[] GetPDF(PdfDomandaModel domandaModel, DateTime dataUltimaModifica)
        {
            MemoryStream outPut = null; ;

            using (outPut = new MemoryStream())
            {
                Font fontArial_06_Normal = PdfFontUtility.ArialFont(6, PdfFontUtility.FontStyle.NORMAL);
                Font fontArial_08_Normal = PdfFontUtility.ArialFont(8, PdfFontUtility.FontStyle.NORMAL);
                Font fontArial_09_Normal = PdfFontUtility.ArialFont(9, PdfFontUtility.FontStyle.NORMAL);
                Font fontArial_10_Normal = PdfFontUtility.ArialFont(10, PdfFontUtility.FontStyle.NORMAL);
                Font fontArial_10_NormalUnderline = PdfFontUtility.ArialFont(10, PdfFontUtility.FontStyle.UNDERLINE);
                Font fontArial_12_Normal = PdfFontUtility.ArialFont(12, PdfFontUtility.FontStyle.NORMAL);

                Font fontArial_06_Bold = PdfFontUtility.ArialFont(6, PdfFontUtility.FontStyle.BOLD);
                Font fontArial_08_Bold = PdfFontUtility.ArialFont(8, PdfFontUtility.FontStyle.BOLD);
                Font fontArial_09_Bold = PdfFontUtility.ArialFont(9, PdfFontUtility.FontStyle.BOLD);
                Font fontArial_10_Bold = PdfFontUtility.ArialFont(10, PdfFontUtility.FontStyle.BOLD);
                Font fontArial_10_BoldUnderline = PdfFontUtility.ArialFont(10, PdfFontUtility.FontStyle.BOLDUNDERLINE);
                Font fontArial_12_Bold = PdfFontUtility.ArialFont(12, PdfFontUtility.FontStyle.BOLD);
                Font fontArial_14_Bold = PdfFontUtility.ArialFont(14, PdfFontUtility.FontStyle.BOLD);

                Paragraph rigaVuota_Arial_06_Bold = new Paragraph(" ", fontArial_08_Bold);
                Paragraph rigaVuota_Arial_08_Bold = new Paragraph(" ", fontArial_08_Bold);
                Paragraph rigaVuota_Arial_09_Bold = new Paragraph(" ", fontArial_09_Bold);
                Paragraph rigaVuota_Arial_10_Bold = new Paragraph(" ", fontArial_10_Bold);
                Paragraph rigaVuota_Arial_12_Bold = new Paragraph(" ", fontArial_12_Bold);

                Font fontTimesNewRoman_9_Bold = PdfFontUtility.TimesNewRomanFontFont(9, PdfFontUtility.FontStyle.BOLD);


                Dictionary<int, string> elencoTipiCorso = domandaModel.ElencoTipiCorso;
                Dictionary<int, string> elencoTipiBrevetto = domandaModel.ElencoTipiBrevetto;
                Dictionary<int, string> elencoTipiPatente = domandaModel.ElencoTipiPatente;
                Dictionary<int, string> elencoTipiCertificazione = domandaModel.ElencoTipiCertificazione;
                Dictionary<int, PdfTipoTitoloStudio> elencoTipiTitoloStudio = domandaModel.ElencoTipiTitoloStudio;
                List<PdfSede> ElencoSediDistaccamenti = domandaModel.ElencoSediDistaccamenti;


                float altezzaMargineSuperiore = 25f;
                float altezzaMargineInferiore = 25f;
                float larghezzaMargineSinistro = 15f;
                float larghezzaMargineDestro = 15f;
                float altezzaMargineIntestazione = 12.7f;
                float altezzaMarginePièDiPagina = 7.1f;
                float larghezzaMargineIntestazione = 10f;
                float larghezzaMarginePièDiPagina = 10f;

                Document doc = new Document(PageSize.A4, PdfUtility.MillimetriToPoints(larghezzaMargineSinistro), PdfUtility.MillimetriToPoints(larghezzaMargineDestro), PdfUtility.MillimetriToPoints(altezzaMargineSuperiore), PdfUtility.MillimetriToPoints(altezzaMargineInferiore));

                PdfWriter writer = PdfWriter.GetInstance(doc, outPut);

                bool newPageDone = false;


                PdfTwoColumnHeaderFooter PageEventHandler = new PdfTwoColumnHeaderFooter();
                PageEventHandler.AltezzaMargineIntestazione = altezzaMargineIntestazione;
                PageEventHandler.LarghezzaMargineIntestazione = larghezzaMargineIntestazione;
                PageEventHandler.AltezzaMarginePièDiPagina = altezzaMarginePièDiPagina;
                PageEventHandler.LarghezzaMarginePièDiPagina = larghezzaMarginePièDiPagina;
                writer.PageEvent = PageEventHandler;
                // Define the page header
                PageEventHandler.IdDomanda = domandaModel.IdDomanda;
                PageEventHandler.VersioneDomanda = domandaModel.VersioneDomanda;
                PageEventHandler.DataInvio = dataUltimaModifica;
                PageEventHandler.Concorso = "CAPO SQUADRA 2018";
                PageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 10, Font.BOLD);
                PageEventHandler.HeaderLeft = "Group";
                PageEventHandler.HeaderRight = "1";




                doc.Open();


                Paragraph paragraphTitoloDomanda = new Paragraph(PdfUtility.MillimetriToPoints(-1.8f), "DOMANDA DI PARTECIPAZIONE AL CONCORSO A CAPO SQUADRA", fontArial_12_Bold);
                paragraphTitoloDomanda.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphTitoloDomanda);
                doc.Add(new Paragraph(" ", fontArial_09_Bold));


                Paragraph paragraphSottotitoloDomanda = new Paragraph();
                paragraphSottotitoloDomanda.Alignment = Element.ALIGN_CENTER;
                Phrase f1 = new Phrase("CONCORSO PER L’ACCESSO ALLA QUALIFICA DI ", fontTimesNewRoman_9_Bold);
                Phrase f2 = new Phrase("CAPO SQUADRA", PdfFontUtility.TimesNewRomanFontFont(10, PdfFontUtility.FontStyle.BOLDUNDERLINE));
                Phrase f3 = new Phrase(" DEL C.N.VV.F. PER N. 1144 POSTI COMPLESSIVI", fontTimesNewRoman_9_Bold);
                paragraphSottotitoloDomanda.Add(f1);
                paragraphSottotitoloDomanda.Add(f2);
                paragraphSottotitoloDomanda.Add(f3);
                doc.Add(paragraphSottotitoloDomanda);


                Paragraph paragraphDecorrenzaDomanda = new Paragraph("DECORRENZA 1.1.2018", PdfFontUtility.TimesNewRomanFontFont(12, PdfFontUtility.FontStyle.BOLDUNDERLINE));
                paragraphDecorrenzaDomanda.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphDecorrenzaDomanda);


                //Paragraph paragraphDecretoMinisterialeDomanda = new Paragraph("(D.M. n. ___ del __/__/2018)", fontArial_12_Normal);
                Paragraph paragraphDecretoMinisterialeDomanda = new Paragraph(" ", fontArial_12_Normal);
                paragraphDecretoMinisterialeDomanda.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphDecretoMinisterialeDomanda);
                doc.Add(new Paragraph(" ", fontArial_08_Bold));
                doc.Add(new Paragraph(" ", fontArial_08_Bold));
                doc.Add(new Paragraph(" ", fontArial_09_Bold));



                string textSottoscritto0 = "II sottoscritto ";
                string textSottoscritto1 = domandaModel.DatiAnagrafica.COGNOME + " " + domandaModel.DatiAnagrafica.NOME;
                string textSottoscritto2 = " chiede di essere ammesso a partecipare alla procedura straordinaria ";
                textSottoscritto2 += "per il passaggio alla qualifica di capo squadra del ruolo dei capi squadra e capi reparto ";
                textSottoscritto2 += "per complessivi n. 1144 posti, di cui n. 1053 da generico, n. 39 da nautico, n. 37 da sommozzatore, ";
                textSottoscritto2 += "n. 11 da radioriparatore, n. 4 da elicotterista, decorrenza 1.1.2018. ";
                Phrase phraseSottoscritto0 = new Phrase(textSottoscritto0, fontArial_10_Normal);
                Phrase phraseSottoscritto1 = new Phrase(textSottoscritto1, fontArial_10_NormalUnderline);
                Phrase phraseSottoscritto2 = new Phrase(textSottoscritto2, fontArial_10_Normal);

                Paragraph paragraphIlSottoscritto1 = new Paragraph();
                paragraphIlSottoscritto1.Alignment = Element.ALIGN_JUSTIFIED;
                paragraphIlSottoscritto1.Add(phraseSottoscritto0);
                paragraphIlSottoscritto1.Add(phraseSottoscritto1);
                paragraphIlSottoscritto1.Add(phraseSottoscritto2);
                doc.Add(paragraphIlSottoscritto1);
                string textSottoscritto3 = "A tal fine, ai sensi dell’articolo 46 del decreto del Presidente della Repubblica 28 dicembre 2000, ";
                textSottoscritto3 += "n. 445 e successive modificazioni e integrazioni, sotto la propria responsabilità e ";
                textSottoscritto3 += "consapevole delle sanzioni penali in caso di falsità in atti o dichiarazioni mendaci ";
                textSottoscritto3 += "previste dall’art. 76 del predetto decreto";
                Paragraph paragraphIlSottoscritto2 = new Paragraph(textSottoscritto3, fontArial_10_Normal);
                paragraphIlSottoscritto2.Alignment = Element.ALIGN_JUSTIFIED;
                doc.Add(paragraphIlSottoscritto2);
                doc.Add(rigaVuota_Arial_10_Bold);


                Paragraph paragraphDichiara = new Paragraph("DICHIARA", fontArial_10_Bold);
                paragraphDichiara.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphDichiara);
                doc.Add(rigaVuota_Arial_10_Bold);




                fontArial_10_Normal.Color = BaseColor.BLACK;
                BaseFont fontCol1With = PdfFontUtility.ArialBaseFont();
                string textCol1With = "Indirizzo di posta elettronica ";
                float col1With = fontCol1With.GetWidthPoint(textCol1With, 10) + PdfUtility.MillimetriToPoints(1f);
                float tableAnagraficaSize = doc.PageSize.Width - doc.LeftMargin - doc.RightMargin;
                float[] widths = new float[] { col1With, tableAnagraficaSize - col1With };
                PdfPTable tableAnagrafica = new PdfPTable(2);
                tableAnagrafica.TotalWidth = tableAnagraficaSize;
                tableAnagrafica.LockedWidth = true;
                tableAnagrafica.SetWidths(widths);
                tableAnagrafica.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableAnagrafica.SpacingBefore = 0f;
                tableAnagrafica.SpacingAfter = 0f;

                float cellsFixedHeight1 = PdfUtility.MillimetriToPoints(5.1f);
                float cellsFixedHeight2 = PdfUtility.MillimetriToPoints(5.0f);
                float cellsFixedHeight = cellsFixedHeight1 + cellsFixedHeight2;

                List<Tuple<string, PdfPCell, string, string, int>> elencoCelleAnagrafica = new List<Tuple<string, PdfPCell, string, string, int>>();

                PdfPCell cellCognome = new PdfPCell(new Phrase("Cognome", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("COGNOME", cellCognome, domandaModel.DatiAnagrafica.COGNOME, string.Empty, 1));

                PdfPCell cellNome = new PdfPCell(new Phrase("Nome", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("NOME", cellNome, domandaModel.DatiAnagrafica.NOME, string.Empty, 1));

                PdfPCell cellDataNascita = new PdfPCell(new Phrase("Data di nascita", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("DATA NASCITA", cellDataNascita, domandaModel.DatiAnagrafica.DTNASCITA.Replace("/", "").Replace("-", ""), string.Empty, 1));

                PdfPCell cellComuneNascita = new PdfPCell(new Phrase("Comune di nascita", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("COMUNE DI NASCITA", cellComuneNascita, domandaModel.DatiAnagrafica.COMUNENASC, domandaModel.DatiAnagrafica.PROVINCIANAS, 1));


                string luogoDomicilio =
                    (domandaModel.DatiAnagrafica.INDIRIZZO.HasText() ? domandaModel.DatiAnagrafica.INDIRIZZO.Trim() : string.Empty) +
                    (domandaModel.DatiAnagrafica.DESCCOMUNE.HasText() ? " - " + domandaModel.DatiAnagrafica.DESCCOMUNE : string.Empty) +
                    (domandaModel.DatiAnagrafica.PROVINCIA.HasText() ? " (" + domandaModel.DatiAnagrafica.PROVINCIA + ")" : string.Empty);

                PdfPCell cellDomicilio = new PdfPCell(new Phrase("Domicilio", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("DOMICILIO", cellDomicilio, luogoDomicilio, string.Empty, 2));

                PdfPCell cellCodiceFiscale = new PdfPCell(new Phrase("Codice fiscale", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("CODICE FISCALE", cellCodiceFiscale, domandaModel.DatiAnagrafica.CODFISCALE, string.Empty, 1));

                PdfPCell cellRecapitoTelefonico = new PdfPCell(new Phrase("Recapito telefonico", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("RECAPITO TELEFONICO", cellRecapitoTelefonico, domandaModel.DatiAnagrafica.PREFTEL1 + " " + domandaModel.DatiAnagrafica.TELEFONO1, string.Empty, 1));

                PdfPCell cellIndirizzoPostaElettronica = new PdfPCell(new Phrase("Indirizzo di posta elettronica", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("INDIRIZZO DI POSTA ELETTRONICA", cellIndirizzoPostaElettronica, domandaModel.DatiAnagrafica.EMAIL, string.Empty, 2));

                PdfPCell cellSedeServizio = new PdfPCell(new Phrase("Sede di servizio", fontArial_10_Normal));
                elencoCelleAnagrafica.Add(new Tuple<string, PdfPCell, string, string, int>("SEDE DI SERVIZIO", cellSedeServizio, domandaModel.DESCRIZIONE_SEDESERVIZIO, string.Empty, 3));


                PdfPTable tableAnagraficaCol1 = new PdfPTable(1);
                tableAnagraficaCol1.DefaultCell.FixedHeight = cellsFixedHeight;
                foreach (var cellaAnagrafica in elencoCelleAnagrafica)
                {
                    cellaAnagrafica.Item2.Border = 0;

                    cellaAnagrafica.Item2.FixedHeight = cellsFixedHeight;

                    for (int rv = 1; rv < cellaAnagrafica.Item5; rv++) cellaAnagrafica.Item2.FixedHeight += cellsFixedHeight1;

                    tableAnagraficaCol1.AddCell(cellaAnagrafica.Item2);
                }

                PdfPCell tableAnagraficaCol1Cell = new PdfPCell(tableAnagraficaCol1);
                tableAnagraficaCol1Cell.Padding = 0f;
                tableAnagraficaCol1Cell.Border = 0;
                tableAnagrafica.AddCell(tableAnagraficaCol1Cell);

                float redCellBorderWidth = 0.1f;
                float redCellBorderNoWidth = 0.0f;
                int maxCols = 42;
                int maxRows = elencoCelleAnagrafica.Count;
                PdfPTable tableAnagraficaCol2 = new PdfPTable(maxCols);
                tableAnagraficaCol2.DefaultCell.FixedHeight = cellsFixedHeight;
                for (int r = 0; r < maxRows; r++)
                {
                    int celleCompilate = 0;

                    for (int m = 0; m < elencoCelleAnagrafica[r].Item5; m++)
                    {
                        for (int c = 0; c < maxCols; c++)
                        {
                            string carattere = string.Empty;

                            string contenutoPerCelle = elencoCelleAnagrafica[r].Item3;

                            PdfPCell cellRed = new PdfPCell();
                            cellRed.FixedHeight = cellsFixedHeight1;
                            cellRed.PaddingTop = 1f;
                            cellRed.PaddingLeft = 1f;
                            cellRed.Border = 1;
                            cellRed.BorderColor = BaseColor.RED;
                            cellRed.BorderWidthTop = redCellBorderWidth;
                            cellRed.BorderColorTop = BaseColor.RED;
                            cellRed.BorderWidthBottom = redCellBorderWidth;
                            cellRed.BorderColorBottom = BaseColor.RED;
                            cellRed.BorderWidthLeft = redCellBorderWidth;
                            cellRed.BorderColorLeft = BaseColor.RED;
                            if (c < maxCols - 1)
                            {
                                cellRed.BorderWidthRight = redCellBorderNoWidth;
                                cellRed.BorderColorRight = BaseColor.WHITE;
                            }
                            else
                            {
                                cellRed.BorderWidthRight = redCellBorderWidth;
                                cellRed.BorderColorRight = BaseColor.RED;
                            }

                            if (m > 0)
                            {
                                cellRed.BorderWidthTop = redCellBorderNoWidth;
                                cellRed.BorderColorTop = BaseColor.WHITE;
                            }

                            if (elencoCelleAnagrafica[r].Item1 == "INDIRIZZO DI POSTA ELETTRONICA")
                            {
                                if (contenutoPerCelle.Length > celleCompilate)
                                {
                                    carattere = contenutoPerCelle.Substring(celleCompilate, 1);
                                    celleCompilate++;

                                    if (carattere == "@")
                                    {
                                        cellRed.PaddingLeft = 0f;
                                    }
                                }
                            }
                            else if (elencoCelleAnagrafica[r].Item1 == "CODICE FISCALE" && c >= 16)
                            {
                                cellRed.BorderWidthTop = redCellBorderNoWidth;
                                cellRed.BorderColorTop = BaseColor.WHITE;
                                cellRed.BorderWidthBottom = redCellBorderNoWidth;
                                cellRed.BorderColorBottom = BaseColor.WHITE;

                                cellRed.BorderWidthRight = redCellBorderNoWidth;
                                cellRed.BorderColorRight = BaseColor.WHITE;
                                if (c > 16)
                                {
                                    cellRed.BorderWidthLeft = redCellBorderNoWidth;
                                    cellRed.BorderColorLeft = BaseColor.WHITE;
                                }

                            }
                            else if (elencoCelleAnagrafica[r].Item1 == "DATA NASCITA" && (c == 2 || c == 5 || c >= 10))
                            {
                                cellRed.BorderWidthTop = redCellBorderNoWidth;
                                cellRed.BorderColorTop = BaseColor.WHITE;
                                cellRed.BorderWidthBottom = redCellBorderNoWidth;
                                cellRed.BorderColorBottom = BaseColor.WHITE;

                                if (c >= 10)
                                {
                                    cellRed.BorderWidthRight = redCellBorderNoWidth;
                                    cellRed.BorderColorRight = BaseColor.WHITE;
                                    if (c > 10)
                                    {
                                        cellRed.BorderWidthLeft = redCellBorderNoWidth;
                                        cellRed.BorderColorLeft = BaseColor.WHITE;
                                    }
                                }
                            }
                            else if (elencoCelleAnagrafica[r].Item1 == "COMUNE DI NASCITA")
                            {
                                int celleDaSaltare = 3;

                                int posizioneLimite = maxCols - celleDaSaltare - 2;

                                if (c < (posizioneLimite))
                                {
                                    if (contenutoPerCelle.Length > celleCompilate)
                                    {
                                        carattere = contenutoPerCelle.Substring(celleCompilate, 1);
                                        celleCompilate++;
                                    }
                                }
                                else if (c == posizioneLimite)
                                {
                                    cellRed.Colspan = celleDaSaltare;

                                    cellRed.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right

                                    cellRed.BorderWidthTop = redCellBorderNoWidth;
                                    cellRed.BorderColorTop = BaseColor.WHITE;
                                    cellRed.BorderWidthBottom = redCellBorderNoWidth;
                                    cellRed.BorderColorBottom = BaseColor.WHITE;
                                    cellRed.Phrase = new Phrase("Prov.", fontArial_10_Normal);
                                    celleCompilate = 0;

                                    c += (celleDaSaltare - 1);
                                }
                                else if (c > posizioneLimite)
                                {
                                    if (elencoCelleAnagrafica[r].Item3.Length > celleCompilate)
                                    {
                                        carattere = elencoCelleAnagrafica[r].Item4.Substring(celleCompilate, 1);
                                        celleCompilate++;
                                    }
                                }
                            }
                            else
                            {
                                if (contenutoPerCelle.Length > celleCompilate)
                                {
                                    carattere = contenutoPerCelle.Substring(celleCompilate, 1);
                                    celleCompilate++;
                                }
                            }


                            Phrase frase = new Phrase(carattere, fontArial_10_Normal);

                            if (carattere == "I")
                            {
                                cellRed.PaddingLeft = 2.0f;
                            }
                            else if (elencoCelleAnagrafica[r].Item1 == "INDIRIZZO DI POSTA ELETTRONICA" && carattere == "@")
                            {
                                cellRed.PaddingTop = 3f;
                                frase = new Phrase(carattere, fontArial_08_Normal);
                            }

                            if (cellRed.Phrase == null) cellRed.Phrase = frase;

                            tableAnagraficaCol2.AddCell(cellRed);
                        }
                    }

                    for (int c = 0; c < maxCols; c++)
                    {
                        PdfPCell cellEmpty = new PdfPCell(new Phrase("", fontArial_10_Normal));
                        cellEmpty.FixedHeight = cellsFixedHeight2;
                        cellEmpty.Border = 0;
                        cellEmpty.BorderWidth = 0;
                        tableAnagraficaCol2.AddCell(cellEmpty);
                    }
                }
                PdfPCell tableAnagraficaCol2Cell = new PdfPCell(tableAnagraficaCol2);
                tableAnagraficaCol2Cell.Border = 0;
                tableAnagrafica.AddCell(tableAnagraficaCol2Cell);



                doc.Add(tableAnagrafica);

                //doc.Add(rigaVuota_Arial_06_Bold);

                Paragraph paragraphRequisitiAmmissione = new Paragraph("− di essere in possesso dei seguenti requisiti di ammissione:", fontArial_10_Bold);
                paragraphRequisitiAmmissione.Alignment = Element.ALIGN_LEFT;
                doc.Add(paragraphRequisitiAmmissione);

                doc.Add(rigaVuota_Arial_06_Bold);

                List listRequisitiAmmissione = new List(false, false, PdfUtility.MillimetriToPoints(5.0f));
                listRequisitiAmmissione.IndentationLeft = PdfUtility.MillimetriToPoints(3.5f);
                listRequisitiAmmissione.SetListSymbol("\t\u2022");

                listRequisitiAmmissione.Add(new ListItem("qualifica di vigile coordinatore alla data del 31.12.2017;", fontArial_10_Bold));

                ListItem sanzioneDisciplinareListItem = new ListItem();
                sanzioneDisciplinareListItem.Add(new Phrase("non avere riportato, ", fontArial_10_Normal));
                sanzioneDisciplinareListItem.Add(new Phrase("nel biennio precedente alla data del 31.12.2017", fontArial_10_Bold));
                sanzioneDisciplinareListItem.Add(new Phrase(", una sanzione disciplinare più grave della sanzione pecuniaria;", fontArial_10_Normal));
                listRequisitiAmmissione.Add(sanzioneDisciplinareListItem);

                if (domandaModel.Specializzazione_Denominazione != null && domandaModel.Specializzazione_Denominazione.Trim() != string.Empty && domandaModel.Specializzazione_Denominazione.Trim().ToUpper() != "NESSUNA")
                {
                    ListItem specializzazioneListItem = new ListItem();
                    specializzazioneListItem.Add(new Phrase("di essere in possesso della specializzazione di ", fontArial_10_Normal));
                    specializzazioneListItem.Add(new Phrase(domandaModel.Specializzazione_Denominazione, fontArial_10_NormalUnderline));
                    specializzazioneListItem.Add(new Phrase(" conseguita il ", fontArial_10_Normal));
                    specializzazioneListItem.Add(new Phrase(domandaModel.Specializzazione_DataConseguimento.ToShortDateString(), fontArial_10_NormalUnderline));
                    specializzazioneListItem.Add(new Phrase(" presso ", fontArial_10_Normal));
                    specializzazioneListItem.Add(new Phrase(domandaModel.Specializzazione_LuogoConseguimento, fontArial_10_NormalUnderline));
                    specializzazioneListItem.Add(new Phrase(" e di essere a conoscenza di poter concorrere ", fontArial_10_Normal));
                    specializzazioneListItem.Add(new Phrase("esclusivamente", fontArial_10_NormalUnderline));
                    specializzazioneListItem.Add(new Phrase(" per i posti e per le sedi dove operano i relativi nuclei specialistici.", fontArial_10_Normal));
                    listRequisitiAmmissione.Add(specializzazioneListItem);

                }

                doc.Add(listRequisitiAmmissione);

                doc.NewPage();

                Paragraph paragraphAnzianità = new Paragraph("ANZIANITA’ DI SERVIZIO", fontArial_10_Bold);
                paragraphAnzianità.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphAnzianità);
                doc.Add(new Paragraph(" ", fontArial_10_Normal));

                List listRequisitiAnzianità = new List(false, false, PdfUtility.MillimetriToPoints(5.0f));
                listRequisitiAnzianità.IndentationLeft = PdfUtility.MillimetriToPoints(3.5f);
                listRequisitiAnzianità.SetListSymbol("\t\u2022");
                ListItem requisitiAnzianitàListItem = new ListItem();
                requisitiAnzianitàListItem.Add(new Phrase("anzianità nel ", fontArial_10_Normal));
                requisitiAnzianitàListItem.Add(new Phrase("ruolo", fontArial_10_Normal));
                requisitiAnzianitàListItem.Add(new Phrase(" dei vigili del fuoco:", fontArial_10_Normal));
                listRequisitiAnzianità.Add(requisitiAnzianitàListItem);
                doc.Add(listRequisitiAnzianità);

                Paragraph paragraphRequisitiAnzianità2 = new Paragraph();
                paragraphRequisitiAnzianità2.Alignment = Element.ALIGN_LEFT;
                paragraphRequisitiAnzianità2.IndentationLeft = PdfUtility.MillimetriToPoints(3.5f);
                paragraphRequisitiAnzianità2.Add(new Phrase("(compresa quella maturata nel corrispondente profilo del previgente ordinamento) dal ", fontArial_10_Normal));
                paragraphRequisitiAnzianità2.Add(new Phrase(domandaModel.DatiCarriera.ANZIANITA, fontArial_10_NormalUnderline));
                doc.Add(paragraphRequisitiAnzianità2);
                doc.Add(rigaVuota_Arial_10_Bold);
                doc.Add(new Paragraph(" ", fontArial_10_Normal));


                Paragraph paragraphCorsiAggiornamento1 = new Paragraph("CORSI DI AGGIORNAMENTO PROFESSIONALE ORGANIZZATI DALL’AMMINISTRAZIONE IN MATERIE", fontArial_10_Bold);
                paragraphCorsiAggiornamento1.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphCorsiAggiornamento1);
                Paragraph paragraphCorsiAggiornamento2 = new Paragraph();
                paragraphCorsiAggiornamento2.Alignment = Element.ALIGN_CENTER;
                paragraphCorsiAggiornamento2.Add(new Phrase("ATTINENTI L’ ATTIVITA’ ISTITUZIONALE E FREQUENTATI CON ", fontArial_10_Bold));
                paragraphCorsiAggiornamento2.Add(new Phrase("PROFITTO", fontArial_10_BoldUnderline));
                doc.Add(paragraphCorsiAggiornamento2);
                doc.Add(new Paragraph(" ", fontArial_10_Normal));
                doc.Add(new Paragraph(" ", fontArial_10_Normal));


                bool first = true;
                foreach (var corso in domandaModel.ElencoCorsiAggiornamento)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphCorsoAggiornamento = new Paragraph();
                    paragraphCorsoAggiornamento.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphCorsoAggiornamento.Add(new Phrase("Tipo di corso ", fontArial_10_Normal));
                    paragraphCorsoAggiornamento.Add(new Phrase(PdfUtility.GetTipo(elencoTipiCorso, corso), fontArial_10_NormalUnderline));
                    paragraphCorsoAggiornamento.Add(new Phrase(" durata ", fontArial_10_Normal));
                    paragraphCorsoAggiornamento.Add(new Phrase((corso.DURATA > 0 ? corso.DURATA.ToString() : "___") + " " + corso.TIPO_DURATA, fontArial_10_NormalUnderline));
                    paragraphCorsoAggiornamento.Add(new Phrase(" svolto dal ", fontArial_10_Normal));
                    paragraphCorsoAggiornamento.Add(new Phrase((corso.DAL.HasText() ? corso.DAL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphCorsoAggiornamento.Add(new Phrase(" al ", fontArial_10_Normal));
                    paragraphCorsoAggiornamento.Add(new Phrase((corso.AL.HasText() ? corso.AL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphCorsoAggiornamento.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphCorsoAggiornamento.Add(new Phrase(PdfUtility.GetLuogo(ElencoSediDistaccamenti, corso.CODSEDE, corso.CODDISTAC, corso.ALTROSITO), fontArial_10_NormalUnderline));
                    paragraphCorsoAggiornamento.Add(new Phrase(" attestato rilasciato da ", fontArial_10_Normal));
                    paragraphCorsoAggiornamento.Add(new Phrase((corso.RILASCIATODA.HasText() ? corso.RILASCIATODA : "________________"), fontArial_10_NormalUnderline));
                    doc.Add(paragraphCorsoAggiornamento);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }

                if (!newPageDone)
                    doc.NewPage();
                else
                    newPageDone = false;




                Paragraph paragraphPatentiBrevettiCertificazioni1 = new Paragraph("CORSI PER L’ACQUISIZIONE DELLE QUALIFICAZIONI RISULTANTI DA APPOSITI BREVETTI O PATENTI", fontArial_10_Bold);
                paragraphPatentiBrevettiCertificazioni1.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphPatentiBrevettiCertificazioni1);
                Paragraph paragraphPatentiBrevettiCertificazioni2 = new Paragraph();
                paragraphPatentiBrevettiCertificazioni2.Alignment = Element.ALIGN_CENTER;
                paragraphPatentiBrevettiCertificazioni2.Add(new Phrase("OVVERO DA CERTIFICAZIONI DELL’AMMINISTRAZIONE", fontArial_10_Bold));
                doc.Add(paragraphPatentiBrevettiCertificazioni2);
                doc.Add(new Paragraph(" ", fontArial_10_Normal));
                doc.Add(new Paragraph(" ", fontArial_10_Normal));



                first = true;
                foreach (var titolo in domandaModel.ElencoBrevetti)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphEntity = new Paragraph();
                    paragraphEntity.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphEntity.Add(new Phrase("Tipo di corso ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase(titolo.TipoEntità.ToLower() + " - " + PdfUtility.GetTipo(elencoTipiBrevetto, titolo), fontArial_10_NormalUnderline));

                    paragraphEntity.Add(new Phrase(" durata ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.DURATA > 0 ? titolo.DURATA.ToString() : "___") + " " + titolo.TIPO_DURATA, fontArial_10_NormalUnderline));

                    paragraphEntity.Add(new Phrase(" svolto dal ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.DAL.HasText() ? titolo.DAL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" al ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.AL.HasText() ? titolo.AL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase(PdfUtility.GetLuogo(ElencoSediDistaccamenti, titolo.CODSEDE, titolo.CODDISTAC, titolo.ALTROSITO), fontArial_10_NormalUnderline));

                    paragraphEntity.Add(new Phrase(" titolo rilasciato da ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.RILASCIATODA.HasText() ? titolo.RILASCIATODA : "________________"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" il ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.RILASCIATOIL.HasText() ? titolo.RILASCIATOIL : "__/__/__"), fontArial_10_NormalUnderline));

                    doc.Add(paragraphEntity);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }


                foreach (var titolo in domandaModel.ElencoPatenti)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphEntity = new Paragraph();
                    paragraphEntity.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphEntity.Add(new Phrase("Tipo di corso ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase(titolo.TipoEntità.ToLower() + " - " + PdfUtility.GetTipo(elencoTipiPatente, titolo), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" durata ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.DURATA > 0 ? titolo.DURATA.ToString() : "___") + " " + titolo.TIPO_DURATA, fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" svolto dal ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.DAL.HasText() ? titolo.DAL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" al ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.AL.HasText() ? titolo.AL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase(PdfUtility.GetLuogo(ElencoSediDistaccamenti, titolo.CODSEDE, titolo.CODDISTAC, titolo.ALTROSITO), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" titolo rilasciato da ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.RILASCIATODA.HasText() ? titolo.RILASCIATODA : "________________"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" il ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.RILASCIATOIL.HasText() ? titolo.RILASCIATOIL : "__/__/__"), fontArial_10_NormalUnderline));
                    doc.Add(paragraphEntity);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }


                foreach (var titolo in domandaModel.ElencoCertificazioni)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphEntity = new Paragraph();
                    paragraphEntity.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphEntity.Add(new Phrase("Tipo di corso ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase(titolo.TipoEntità.ToLower() + " - " + PdfUtility.GetTipo(elencoTipiCertificazione, titolo), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" durata ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.DURATA > 0 ? titolo.DURATA.ToString() : "___") + " " + titolo.TIPO_DURATA, fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" svolto dal ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.DAL.HasText() ? titolo.DAL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" al ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.AL.HasText() ? titolo.AL : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase(PdfUtility.GetLuogo(ElencoSediDistaccamenti, titolo.CODSEDE, titolo.CODDISTAC, titolo.ALTROSITO), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" titolo rilasciato da ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.RILASCIATODA.HasText() ? titolo.RILASCIATODA : "________________"), fontArial_10_NormalUnderline));
                    paragraphEntity.Add(new Phrase(" il ", fontArial_10_Normal));
                    paragraphEntity.Add(new Phrase((titolo.RILASCIATOIL.HasText() ? titolo.RILASCIATOIL : "__/__/__"), fontArial_10_NormalUnderline));
                    doc.Add(paragraphEntity);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }



                if (!newPageDone)
                    doc.NewPage();
                else
                    newPageDone = false;

                PdfUtility.GetPositionInPage(writer);


                Paragraph paragraphTitoliStudio = new Paragraph("TITOLI DI STUDIO POSSEDUTI", fontArial_10_Bold);
                paragraphTitoliStudio.Alignment = Element.ALIGN_CENTER;
                doc.Add(paragraphTitoliStudio);
                doc.Add(new Paragraph(" ", fontArial_10_Normal));
                doc.Add(new Paragraph(" ", fontArial_10_Normal));

                first = true;

                List<TitoloDiStudio> elencoTitoliCategoriaDiplomaSecondariaSuperiore = new List<TitoloDiStudio>();
                List<TitoloDiStudio> elencoTitoliCategoriaDiplomaSecondariaSecondoGrado = new List<TitoloDiStudio>();
                List<TitoloDiStudio> elencoTitoliCategoriaLaurea = new List<TitoloDiStudio>();
                List<TitoloDiStudio> elencoTitoliCategoriaLaureaMagistrale = new List<TitoloDiStudio>();
                List<TitoloDiStudio> elencoTitoliCategoriaSpecializzazionePostUniversità = new List<TitoloDiStudio>();

                foreach (var titoloStudio in domandaModel.ElencoTitoli)
                {
                    PdfTipoTitoloStudio pdfTipoTitoloStudio = PdfUtility.GetTipoTitoloStudio(elencoTipiTitoloStudio, titoloStudio);

                    if (pdfTipoTitoloStudio.DescrizioneCategoriaTitoloStudio == "DIPLOMA SECONDARIA SUPERIORE")
                    {
                        elencoTitoliCategoriaDiplomaSecondariaSuperiore.Add(titoloStudio);
                    }
                    else if (pdfTipoTitoloStudio.DescrizioneCategoriaTitoloStudio == "DIPLOMA SECONDARIA SECONDO GRADO")
                    {
                        elencoTitoliCategoriaDiplomaSecondariaSecondoGrado.Add(titoloStudio);
                    }
                    else if (pdfTipoTitoloStudio.DescrizioneCategoriaTitoloStudio == "LAUREA")
                    {
                        elencoTitoliCategoriaLaurea.Add(titoloStudio);
                    }
                    else if (pdfTipoTitoloStudio.DescrizioneCategoriaTitoloStudio == "LAUREA MAGISTRALE")
                    {
                        elencoTitoliCategoriaLaureaMagistrale.Add(titoloStudio);
                    }
                    else if (pdfTipoTitoloStudio.DescrizioneCategoriaTitoloStudio == "DIPLOMA SPECIALIZZAZIONE POST UNIV.")
                    {
                        elencoTitoliCategoriaSpecializzazionePostUniversità.Add(titoloStudio);
                    }
                }

                foreach (var titoloStudio in elencoTitoliCategoriaDiplomaSecondariaSuperiore)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphTitoloStudio = new Paragraph();
                    paragraphTitoloStudio.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphTitoloStudio.Add(new Phrase("Diploma rilasciato da istituto di istruzione secondaria superiore ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.TITOLO.HasText() ? titoloStudio.TITOLO : "__________________"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" conseguito il ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.DATACONSEGUIMENTO.HasText() ? titoloStudio.DATACONSEGUIMENTO : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.ISTITUTO.HasText() ? titoloStudio.ISTITUTO : "__________________"), fontArial_10_NormalUnderline));

                    doc.Add(paragraphTitoloStudio);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }

                foreach (var titoloStudio in elencoTitoliCategoriaDiplomaSecondariaSecondoGrado)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphTitoloStudio = new Paragraph();
                    paragraphTitoloStudio.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphTitoloStudio.Add(new Phrase("Diploma di istruzione secondaria di secondo grado ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.TITOLO.HasText() ? titoloStudio.TITOLO : "__________________"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" conseguito il ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.DATACONSEGUIMENTO.HasText() ? titoloStudio.DATACONSEGUIMENTO : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.ISTITUTO.HasText() ? titoloStudio.ISTITUTO : "__________________"), fontArial_10_NormalUnderline));

                    doc.Add(paragraphTitoloStudio);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }

                foreach (var titoloStudio in elencoTitoliCategoriaLaurea)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphTitoloStudio = new Paragraph();
                    paragraphTitoloStudio.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphTitoloStudio.Add(new Phrase("Laurea ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.TITOLO.HasText() ? titoloStudio.TITOLO : "__________________"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" conseguita il ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.DATACONSEGUIMENTO.HasText() ? titoloStudio.DATACONSEGUIMENTO : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.ISTITUTO.HasText() ? titoloStudio.ISTITUTO : "__________________"), fontArial_10_NormalUnderline));

                    doc.Add(paragraphTitoloStudio);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }

                foreach (var titoloStudio in elencoTitoliCategoriaLaureaMagistrale)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphTitoloStudio = new Paragraph();
                    paragraphTitoloStudio.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphTitoloStudio.Add(new Phrase("Laurea magistrale ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.TITOLO.HasText() ? titoloStudio.TITOLO : "__________________"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" conseguita il ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.DATACONSEGUIMENTO.HasText() ? titoloStudio.DATACONSEGUIMENTO : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.ISTITUTO.HasText() ? titoloStudio.ISTITUTO : "__________________"), fontArial_10_NormalUnderline));

                    doc.Add(paragraphTitoloStudio);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }

                foreach (var titoloStudio in elencoTitoliCategoriaSpecializzazionePostUniversità)
                {
                    if (!first) doc.Add(new Paragraph(" ", fontArial_10_Normal));
                    first = false;

                    Paragraph paragraphTitoloStudio = new Paragraph();
                    paragraphTitoloStudio.Alignment = Element.ALIGN_JUSTIFIED;
                    paragraphTitoloStudio.Add(new Phrase("Diploma di specializzazione conseguito al termine di corsi di specializzazione istituiti dalle Università ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.TITOLO.HasText() ? titoloStudio.TITOLO : "__________________"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" conseguito il ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.DATACONSEGUIMENTO.HasText() ? titoloStudio.DATACONSEGUIMENTO : "__/__/__"), fontArial_10_NormalUnderline));
                    paragraphTitoloStudio.Add(new Phrase(" presso ", fontArial_10_Normal));
                    paragraphTitoloStudio.Add(new Phrase((titoloStudio.ISTITUTO.HasText() ? titoloStudio.ISTITUTO : "__________________"), fontArial_10_NormalUnderline));

                    doc.Add(paragraphTitoloStudio);

                    newPageDone = PdfUtility.ManageNewPage(writer, doc);
                    if (newPageDone) first = true;
                }




                int jumpLines = 0;
                int[] jumpArray = { 0, 0, 0 };

                if (PdfUtility.GetPositionInPage(writer) < 166.0f)
                {
                    float diff = 166.0f - PdfUtility.GetPositionInPage(writer);
                    float linees = diff / 5.2917f;
                    jumpLines = (int)Math.Round(linees);
                }

                if (jumpLines > 10)
                {
                    doc.NewPage();

                    jumpLines = 2;
                }

                if (jumpLines > 0)
                {
                    int jumpIdx = 0;

                    while (jumpLines > 0)
                    {
                        if (jumpIdx == 0 && jumpArray[0] >= 2)
                        {
                            jumpIdx++;
                            if (jumpIdx >= jumpArray.Count()) jumpIdx = 0;
                        }

                        jumpArray[jumpIdx]++;

                        jumpLines--;

                        jumpIdx++;
                        if (jumpIdx >= jumpArray.Count()) jumpIdx = 0;
                    }
                }


                // BLOCCO ACCAPI DINAMICO
                for (int newLineCounter = 0; newLineCounter < (5 - jumpArray[0]); newLineCounter++)
                {
                    doc.Add(new Paragraph(" ", fontArial_10_Normal));
                }



                float tableDataFirmaSize = doc.PageSize.Width - doc.LeftMargin - doc.RightMargin;
                float tableDataFirmaSizeCol1 = tableDataFirmaSize / 2;
                float tableDataFirmaSizeCol2 = tableDataFirmaSize - tableDataFirmaSizeCol1;
                float[] widthsDataFirma = new float[] { tableDataFirmaSizeCol1, tableDataFirmaSizeCol2 };
                PdfPTable tableDataFirma = new PdfPTable(2);
                tableDataFirma.TotalWidth = tableDataFirmaSize;
                tableDataFirma.LockedWidth = true;
                tableDataFirma.SetWidths(widths);
                tableDataFirma.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                tableDataFirma.SpacingBefore = 0f;
                tableDataFirma.SpacingAfter = 0f;



                Paragraph paragraphData = new Paragraph("data ___________", fontArial_10_Normal);
                paragraphData.Alignment = Element.ALIGN_LEFT;
                PdfPCell cellData = new PdfPCell(paragraphData);
                cellData.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cellData.BorderWidthTop = redCellBorderNoWidth;
                cellData.BorderColorTop = BaseColor.WHITE;
                cellData.BorderWidthBottom = redCellBorderNoWidth;
                cellData.BorderColorBottom = BaseColor.WHITE;
                cellData.BorderWidthLeft = redCellBorderNoWidth;
                cellData.BorderColorLeft = BaseColor.WHITE;
                cellData.BorderWidthRight = redCellBorderNoWidth;
                cellData.BorderColorRight = BaseColor.WHITE;

                Paragraph paragraphFirma = new Paragraph("firma _________________________________", fontArial_10_Normal);
                paragraphFirma.Alignment = Element.ALIGN_RIGHT;
                PdfPCell cellFirma = new PdfPCell(paragraphFirma);
                cellFirma.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                cellFirma.BorderWidthTop = redCellBorderNoWidth;
                cellFirma.BorderColorTop = BaseColor.WHITE;
                cellFirma.BorderWidthBottom = redCellBorderNoWidth;
                cellFirma.BorderColorBottom = BaseColor.WHITE;
                cellFirma.BorderWidthLeft = redCellBorderNoWidth;
                cellFirma.BorderColorLeft = BaseColor.WHITE;
                cellFirma.BorderWidthRight = redCellBorderNoWidth;
                cellFirma.BorderColorRight = BaseColor.WHITE;

                tableDataFirma.AddCell(cellData);
                tableDataFirma.AddCell(cellFirma);
                doc.Add(tableDataFirma);

                // BLOCCO ACCAPI DINAMICO
                for (int newLineCounter = 0; newLineCounter < (5 - jumpArray[1]); newLineCounter++)
                {
                    doc.Add(new Paragraph(" ", fontArial_10_Normal));
                }

                Paragraph paragraphCompletezza = new Paragraph("La completezza e correttezza delle informazioni richieste è necessaria per le verifiche a carico dell’Amministrazione.", fontArial_10_Bold);
                paragraphCompletezza.Alignment = Element.ALIGN_JUSTIFIED;
                doc.Add(paragraphCompletezza);

                // BLOCCO ACCAPI DINAMICO
                for (int newLineCounter = 0; newLineCounter < (5 - jumpArray[2]); newLineCounter++)
                {
                    doc.Add(new Paragraph(" ", fontArial_10_Normal));
                }

                //Paragraph paragraphArticolo = new Paragraph("Ai sensi dell’art.__ del decreto legislativo del 30 giugno 2003 n.196 e sue modifiche ed integrazioni, si informa che il trattamento dei dati personali forniti dai candidati avverrà, anche con strumenti informatici, esclusivamente per le finalità della procedura concorsuale. ", fontArial_10_Normal);
                Paragraph paragraphArticolo = new Paragraph("Ai sensi del decreto legislativo del 30 giugno 2003 n.196 e sue modifiche ed integrazioni, si informa che il trattamento dei dati personali forniti dai candidati avverrà, anche con strumenti informatici, esclusivamente per le finalità della procedura concorsuale. Il conferimento dei dati è obbligatorio ai fini della verifica dei requisiti di partecipazione e della valutazione dei titoli dei candidati. Esclusivamente ai fini della verifica, i dati forniti potranno essere portati a conoscenza degli enti di volta in volta interessati. I candidati hanno facoltà di esercitare in qualunque momento i diritti di cui all'art.9 del bando di concorso.", fontArial_10_Normal);
                paragraphArticolo.Alignment = Element.ALIGN_JUSTIFIED;
                doc.Add(paragraphArticolo);

                //doc.Add(new Paragraph(" ", fontArial_10_Normal));
                //doc.Add(new Paragraph("[Dichiaro di aver preso visione del ...]", fontArial_10_Normal));
                //doc.Add(new Paragraph(" ", fontArial_06_Normal));
                //doc.Add(new Paragraph("[Dichiaro di aver preso visione del ...]", fontArial_10_Normal));

                doc.Close();
            }


            return outPut.ToArray();
        }

        static public bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            bool result = false;

            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                }

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        static public List<KeyValuePair<int, string>> GetTipologieTStuAssenti(cConnection connection, List<KeyValuePair<int, string>> listaTipologieTStu)
        {
            List<KeyValuePair<int, string>> result = new List<KeyValuePair<int, string>>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                foreach (KeyValuePair<int, string> elemento in listaTipologieTStu)
                {
                    command = new OracleCommand("SELECT ID_TIPOTITOLOSTUD FROM TB0_TIPOTITOLOSTUD WHERE TIPOTITOLOSTUD = :descrTitolo", connection.Conn);

                    command.Parameters.Add("descrTitolo", elemento.Value);

                    dataReader = command.ExecuteReader();

                    if (dataReader != null)
                    {
                        if (!dataReader.HasRows)
                        {
                            result.Add(elemento);
                        }

                        dataReader.Close();

                        dataReader.Dispose();

                        dataReader = null;

                        command.Dispose();

                        command = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetTipologieTStuAssenti()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public string GetAccountNameFromCF(cConnection connection, string codiceFiscale, int idConcorso, ref string idDomanda, ref string idDomandaConcorso, ref string idUtente, ref DateTime dataUltimaModifica)
        {
            string result = string.Empty;

            string accountName = string.Empty;

            idDomanda = string.Empty;

            idDomandaConcorso = string.Empty;

            idUtente = string.Empty;

            dataUltimaModifica = DateTime.Now;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();


                string encCodiceFiscale = Cryptography.CryptToTextWithSpaces(codiceFiscale);

                command = new OracleCommand("SELECT ACCOUNTNAME, ID_DOMANDA_CONCORSO, TCO_DOMANDA.ID_DOMANDA, DATAUMOD FROM TCO_DOMANDA JOIN TRL_DOMANDA_CONCORSO ON TRL_DOMANDA_CONCORSO.ID_DOMANDA = TCO_DOMANDA.ID_DOMANDA WHERE CODFISCALE = :codiceFiscale AND ID_CONCORSO = :idConcorso", connection.Conn);

                command.Parameters.Add("codiceFiscale", encCodiceFiscale);

                command.Parameters.Add("idConcorso", idConcorso);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        if (dataReader.Read())
                        {
                            accountName = Cryptography.DecryptFromTextWithSpaces(dataReader.GetString(0));

                            idDomandaConcorso = Convert.ToString(dataReader.GetInt32(1));

                            idDomanda = Convert.ToString(dataReader.GetInt32(2));

                            dataUltimaModifica = dataReader.GetDateTime(3);

                            result = accountName;
                        }
                    }

                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                command.Dispose();

                command = null;


                if (result.HasText())
                {
                    command = new OracleCommand("SELECT ID_UTENTE FROM TSY_UTENTE WHERE UPPER(TRIM(USERID)) = :userid", connection.Conn);

                    command.Parameters.Add("userid", result.ToUpper());

                    idUtente = Convert.ToString(command.ExecuteScalar());

                    command.Dispose();

                    command = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetIdConcorso()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }

            return result;
        }

        static public int GetIdConcorso(cConnection connection, string tipoConcorsoProcedura, int anno)
        {
            int result = -1;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("" +
                    "SELECT TB0_CONCORSO.ID_CONCORSO " +
                    "FROM TB0_CONCORSO " +
                    "JOIN TB0_PROCEDURA ON TB0_CONCORSO.ID_PROCEDURA = TB0_PROCEDURA.ID_PROCEDURA " +
                    "WHERE TB0_PROCEDURA.TIPO_CONCORSO = '" + tipoConcorsoProcedura + "' AND EXTRACT(YEAR FROM DATADECORRENZA) = :annoDecorrenza", connection.Conn);

                command.Parameters.Add("annoDecorrenza", anno);

                result = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetIdConcorso()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }

            return result;
        }

        static public bool GetConcorsoAttivo(cConnection connection, string tipoConcorsoProcedura, int anno)
        {
            bool result = false;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("" +
                    "SELECT count(TB0_CONCORSO.ID_CONCORSO) " +
                    "FROM TB0_CONCORSO " +
                    "JOIN TB0_PROCEDURA ON TB0_CONCORSO.ID_PROCEDURA = TB0_PROCEDURA.ID_PROCEDURA " +
                    "WHERE TB0_PROCEDURA.TIPO_CONCORSO = '" + tipoConcorsoProcedura + "' AND EXTRACT(YEAR FROM DATADECORRENZA) = :annoDecorrenza " +
                    "AND TB0_CONCORSO.NASCOSTO = 0 AND TB0_CONCORSO.BLOCCATO = 0 ", connection.Conn);

                command.Parameters.Add("annoDecorrenza", anno);

                result = Convert.ToInt32(command.ExecuteScalar()) == 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetIdConcorso()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }

            return result;
        }

        static public bool GetConcorsoConTerminiScaduti(cConnection connection, string tipoConcorsoProcedura, int anno, out DateTime? dataScadenza)
        {
            bool result = false;

            dataScadenza = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("" +
                    "SELECT DATAFINEPRESENTAZIONE " +
                    "FROM TB0_CONCORSO " +
                    "JOIN TB0_PROCEDURA ON TB0_CONCORSO.ID_PROCEDURA = TB0_PROCEDURA.ID_PROCEDURA " +
                    "WHERE TB0_PROCEDURA.TIPO_CONCORSO = '" + tipoConcorsoProcedura + "' AND EXTRACT(YEAR FROM DATADECORRENZA) = :annoDecorrenza "
                    , connection.Conn);

                command.Parameters.Add("annoDecorrenza", anno);

                DateTime dataFinePresentazione = Convert.ToDateTime(command.ExecuteScalar());

                DateTime giornoSuccessivoAllaScadenza = dataFinePresentazione.AddDays(1);

                DateTime dataAfter = new DateTime(giornoSuccessivoAllaScadenza.Year, giornoSuccessivoAllaScadenza.Month, giornoSuccessivoAllaScadenza.Day);

                dataScadenza = dataAfter.AddTicks(-1);

                result = (DateTime.Now > dataScadenza);
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetConcorsoConTerminiScaduti()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }

            return result;
        }

        static public void GetMaxConseguimentoTitoli(cConnection connection, string tipoConcorsoProcedura, int anno, out DateTime? dataNotValidConseguimentoTitoli)
        {
            dataNotValidConseguimentoTitoli = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("" +
                    "SELECT DATATITOLI " +
                    "FROM TB0_CONCORSO " +
                    "JOIN TB0_PROCEDURA ON TB0_CONCORSO.ID_PROCEDURA = TB0_PROCEDURA.ID_PROCEDURA " +
                    "WHERE TB0_PROCEDURA.TIPO_CONCORSO = '" + tipoConcorsoProcedura + "' AND EXTRACT(YEAR FROM DATADECORRENZA) = :annoDecorrenza "
                    , connection.Conn);

                command.Parameters.Add("annoDecorrenza", anno);

                DateTime DATATITOLI = Convert.ToDateTime(command.ExecuteScalar());

                DateTime giornoSuccessivoAllaDATATITOLI = DATATITOLI.AddDays(1);

                DateTime dataAfter = new DateTime(giornoSuccessivoAllaDATATITOLI.Year, giornoSuccessivoAllaDATATITOLI.Month, giornoSuccessivoAllaDATATITOLI.Day);

                //dataMaxConseguimentoTitoli = dataAfter.AddTicks(-1);
                dataNotValidConseguimentoTitoli = dataAfter;
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetConcorsoConTerminiScaduti()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }
        }

        static public int GetIdDomandaConcorso(cConnection connection, string idDomanda, string idConcorso)
        {
            int result = -1;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT ID_DOMANDA_CONCORSO FROM TRL_DOMANDA_CONCORSO WHERE ID_CONCORSO = :idConcorso AND ID_DOMANDA = :idDomanda", connection.Conn);

                command.Parameters.Add("idConcorso", idConcorso);

                command.Parameters.Add("idDomanda", idDomanda);

                result = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetIdDomandaConcorso()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();

                    connection = null;
                }
            }

            return result;
        }
        static public List<KeyValuePair<int, string>> GetTipoTitolo(string tipoTitolo)
        {
            List<KeyValuePair<int, string>> listaTipi = new List<KeyValuePair<int, string>>();

            cConnection connection = null;

            OracleCommand command = null;

            OracleDataReader dataReader = null;

            try
            {
                connection = new cConnection();

                switch (tipoTitolo)
                {
                    case "Corsi":
                        dataReader = connection.GetDataReader("SELECT * FROM TB0_TIPOCORSO ORDER BY ID_TIPOCORSO");
                        break;

                    case "Patenti":
                        dataReader = connection.GetDataReader("SELECT * FROM TB0_TIPOPATENTE ORDER BY ID_TIPOPATENTE");
                        break;

                    case "Certificazioni":
                        dataReader = connection.GetDataReader("SELECT * FROM TB0_TIPOCERTIFICAZIONE ORDER BY ID_TIPOCERTIFICAZIONE");
                        break;

                    case "Brevetti":
                        dataReader = connection.GetDataReader("SELECT * FROM TB0_TIPOBREVETTO ORDER BY ID_TIPOBREVETTO");
                        break;

                    case "Master":
                        dataReader = connection.GetDataReader("SELECT * FROM TB0_TIPOMASTER ORDER BY ID_TIPOMASTER");
                        break;
                }

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        listaTipi.Add(new KeyValuePair<int, string>(dataReader.GetInt32(0), dataReader.GetString(1)));
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetTipoTitolo()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (connection != null)
                {
                    connection.Dispose();

                    connection = null;
                }
            }

            return listaTipi;
        }

        static public int GetIdTipoAltro(string tipoTitolo, cConnection connection = null)
        {
            int result = 0;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                string nomeCampoDb_IdTipo = string.Empty;
                string nomeCampoDb_Tipo = string.Empty;
                string nomeTabellaDb_Tipo = string.Empty;

                switch (tipoTitolo)
                {
                    case "Corsi":
                        nomeCampoDb_IdTipo = "ID_TIPOCORSO";
                        nomeTabellaDb_Tipo = "TB0_TIPOCORSO";
                        nomeCampoDb_Tipo = "TIPOCORSO";
                        break;

                    case "Patenti":
                        nomeCampoDb_IdTipo = "ID_TIPOPATENTE";
                        nomeTabellaDb_Tipo = "TB0_TIPOPATENTE";
                        nomeCampoDb_Tipo = "TIPOPATENTE";
                        break;

                    case "Certificazioni":
                        nomeCampoDb_IdTipo = "ID_TIPOCERTIFICAZIONE";
                        nomeTabellaDb_Tipo = "TB0_TIPOCERTIFICAZIONE";
                        nomeCampoDb_Tipo = "TIPOCERTIFICAZIONE";
                        break;

                    case "Brevetti":
                        nomeCampoDb_IdTipo = "ID_TIPOBREVETTO";
                        nomeTabellaDb_Tipo = "TB0_TIPOBREVETTO";
                        nomeCampoDb_Tipo = "TIPOBREVETTO";
                        break;

                    case "Master":
                        nomeCampoDb_IdTipo = "ID_TIPOMASTER";
                        nomeTabellaDb_Tipo = "TB0_TIPOMASTER";
                        nomeCampoDb_Tipo = "TIPOMASTER";
                        break;

                }

                command = new OracleCommand("SELECT " + nomeCampoDb_IdTipo + " FROM " + nomeTabellaDb_Tipo + " WHERE UPPER(TRIM(" + nomeCampoDb_Tipo + ")) = 'ALTRO'", connection.Conn);

                result = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo DbUtility:GetIdTipoAltro()", ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<KeyValuePair<int, string>> GetTipologieTStu(cConnection connection)
        {
            List<KeyValuePair<int, string>> result = new List<KeyValuePair<int, string>>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT ID, DESCRIZIONE FROM TSTU_TIPOLOGIA", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(new KeyValuePair<int, string>(dataReader.GetInt32(0), dataReader.GetString(1)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetTipologieTStu()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<KeyValuePair<int, string>> GetTipologieSpecializzazioni(cConnection connection)
        {
            List<KeyValuePair<int, string>> result = new List<KeyValuePair<int, string>>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT ID_TIPOSPECIALIZ, TIPOSPECIALIZ FROM TB0_TIPOSPECIALIZ ORDER BY TIPOSPECIALIZ", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(new KeyValuePair<int, string>(dataReader.GetInt32(0), dataReader.GetString(1)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetTipologieSpecializzazioni()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<KeyValuePair<int, string>> GetComuni(cConnection connection)
        {
            List<KeyValuePair<int, string>> result = null;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                result = new List<KeyValuePair<int, string>>();

                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT CODCOMUNE, DESCCOMUNE || ' (' || CODPROV || ')' FROM TAN_COMUNE ORDER BY DESCCOMUNE", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(new KeyValuePair<int, string>(dataReader.GetInt32(0), dataReader.GetString(1)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetComuni()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

        static public List<KeyValuePair<string, string>> GetElencoSedi(cConnection connection)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT CODSEDE, DESCSEDE FROM TB0_SEDE ORDER BY DESCSEDE", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(new KeyValuePair<string, string>(dataReader.GetString(0), dataReader.GetString(1)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetElencoSedi()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }


        static public List<string> GetCodiciFiscali(cConnection connection)
        {
            List<string> result = null;

            OracleDataReader dataReader = null;

            OracleCommand command = null;

            bool closeConnection = (connection == null);

            try
            {
                result = new List<string>();

                if (connection == null) connection = new cConnection();

                command = new OracleCommand("SELECT CF FROM TMP_INVII_AAGG WHERE CODICE_ESITO IS NULL OR CODICE_ESITO = 0", connection.Conn);

                dataReader = command.ExecuteReader();

                if (dataReader != null)
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            result.Add(dataReader.GetString(0));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Errore nel metodo BaseController:GetCodiciFiscali()", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();

                    dataReader.Dispose();

                    dataReader = null;
                }

                if (command != null)
                {
                    command.Dispose();

                    command = null;
                }

                if (connection != null && closeConnection)
                {
                    connection.Dispose();
                    connection = null;
                }
            }

            return result;
        }

    }
}
