﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Oracle.ManagedDataAccess.Client;
//using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace GestioneConcorsi.DAO
{
    public class cConnection : IDisposable
    {

        public const bool EW_DEBUG_ENABLED = false; // True to debug / False to skip

        public static string ConnectionString = Utils.Cryptography.DecriptConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        //public static string ConnectionString = "User Id=dbo_geconco_fase_3;Password=dbo_geconco_fase_3;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=tcp)(HOST=192.168.1.130)(PORT=1521)))(CONNECT_DATA=(SERVER = DEDICATED)(SERVICE_NAME=ORCL)));";

        public static string EW_DB_SCHEMA = Utils.Cryptography.DecryptFromTextWithSpaces(System.Configuration.ConfigurationManager.AppSettings["DB_Schema"].ToString());

        public OracleConnection Conn;

        public OracleTransaction Trans;

        private OracleConnection TempConn;

        private OracleCommand TempCommand;

        private OracleDataReader TempDataReader;

        // Constructor
        public cConnection(string ConnStr)
        {
            ConnectionString = ConnStr;
            Database_Connecting(ref ConnectionString);
            Conn = new OracleConnection(ConnectionString);
            Conn.Open();
            Execute("ALTER SESSION SET CURRENT_SCHEMA = " + EW_DB_SCHEMA); // Set current schema
            Database_Connected();
        }

        // Constructor
        public cConnection(string ConnStr, string DB_Schema)
        {
            ConnectionString = ConnStr;
            Database_Connecting(ref ConnectionString);
            Conn = new OracleConnection(ConnectionString);
            Conn.Open();
            Execute("ALTER SESSION SET CURRENT_SCHEMA = " + DB_Schema); // Set current schema
            Database_Connected();
        }

        // Constructor
        public cConnection() : this(ConnectionString)
        {
        }

        // Execute SQL
        public int Execute(string Sql)
        {
            OracleCommand Cmd = GetCommand(Sql);
            return Cmd.ExecuteNonQuery();
        }

        // Execute SQL and return first value of first row
        public object ExecuteScalar(string Sql)
        {
            OracleCommand Cmd = GetCommand(Sql);
            return Cmd.ExecuteScalar();
        }

        // Get last insert ID
        public object GetLastInsertId()
        {
            object Id = System.DBNull.Value;
            return Id;
        }

        // Get data reader
        public OracleDataReader GetDataReader(string Sql)
        {
            try
            {
                OracleCommand Cmd = new OracleCommand();
                Cmd = GetCommand(Sql);
                return Cmd.ExecuteReader();
            }
            catch
            {
                if (EW_DEBUG_ENABLED) throw;
                return null;
            }
        }

        // Get temporary data reader
        public OracleDataReader GetTempDataReader(string Sql)
        {
            try
            {
                if (TempConn == null)
                {
                    TempConn = new OracleConnection(ConnectionString);
                    TempConn.Open();
                }
                if (TempCommand == null)
                    TempCommand = new OracleCommand(Sql, TempConn);
                CloseTempDataReader();
                TempCommand.CommandText = Sql;
                TempDataReader = TempCommand.ExecuteReader();
                return TempDataReader;
            }
            catch
            {
                if (EW_DEBUG_ENABLED) throw;
                return null;
            }
        }

        // Close temporary data reader
        public void CloseTempDataReader()
        {
            if (TempDataReader != null)
            {
                TempDataReader.Close();
                TempDataReader.Dispose();
            }
        }

        //// Get OrderedDictionary from data reader
        //public OrderedDictionary GetRow(ref OracleDataReader dr)
        //{
        //    if (dr != null)
        //    {
        //        OrderedDictionary od = new OrderedDictionary();
        //        for (int i = 0; i <= dr.FieldCount - 1; i++)
        //        {
        //            try
        //            {
        //                if (ew_NotEmpty(dr.GetName(i)))
        //                {
        //                    od[dr.GetName(i)] = dr[i];
        //                }
        //                else
        //                {
        //                    od[i] = dr[i];
        //                }
        //            }
        //            catch { 
        //               
        //            }
        //        }
        //        return od;
        //    }
        //    return null;
        //}

        //// Get rows
        //public ArrayList GetRows(ref OracleDataReader dr)
        //{
        //    if (dr != null)
        //    {
        //        ArrayList Rows = new ArrayList();
        //        while (dr.Read())
        //        {
        //            Rows.Add(GetRow(ref dr));
        //        }
        //        return Rows;
        //    }
        //    return null;
        //}

        //// Get rows by SQL
        //public ArrayList GetRows(string Sql)
        //{
        //    OracleDataReader dr = GetTempDataReader(Sql);
        //    try
        //    {
        //        return GetRows(ref dr);
        //    }
        //    finally
        //    {
        //        CloseTempDataReader();
        //    }
        //}

        // Get dataset
        public DataSet GetDataSet(string Sql)
        {
            try
            {
                OracleDataAdapter Adapter = new OracleDataAdapter(Sql, Conn);
                DataSet DS = new DataSet();
                Adapter.Fill(DS);
                return DS;
            }
            catch
            {
                if (EW_DEBUG_ENABLED) throw;
                return null;
            }
        }

        // Get count (by dataset)
        public int GetCount(string Sql)
        {
            DataSet DS = GetDataSet(Sql);
            if (DS != null)
            {
                return DS.Tables[0].Rows.Count;
            }
            else
            {
                return 0;
            }
        }

        // Get command
        public OracleCommand GetCommand(string Sql)
        {
            OracleCommand Cmd = new OracleCommand(Sql, Conn);
            if (Trans != null) Cmd.Transaction = Trans;
            return Cmd;
        }

        // Begin transaction
        public void BeginTrans()
        {
            try
            {
                Trans = Conn.BeginTransaction();
            }
            catch
            {
                if (EW_DEBUG_ENABLED) throw;
            }
        }

        // Commit transaction
        public void CommitTrans()
        {
            if (Trans != null) Trans.Commit();
        }

        // Rollback transaction
        public void RollbackTrans()
        {
            if (Trans != null) Trans.Rollback();
        }

        // Dispose
        public void Dispose()
        {
            if (Trans != null) Trans.Dispose();
            Conn.Close();
            Conn.Dispose();
            if (TempCommand != null) TempCommand.Dispose();
            if (TempConn != null)
            {
                TempConn.Close();
                TempConn.Dispose();
            }
        }

        // Database Connecting event
        public void Database_Connecting(ref string Connstr)
        {

            //HttpContext.Current.Response.Write("Database Connecting");
        }

        // Database Connected event
        public void Database_Connected()
        {

            //Execute("Your SQL");
        }

        // Check if not empty string
        public static bool ew_NotEmpty(object value)
        {
            return !ew_Empty(value);
        }

        // Check if empty string
        public static bool ew_Empty(object value)
        {
            return string.Equals(Convert.ToString(value).Trim(), string.Empty);
        }
    }
}